<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportData;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\BvLog;
use App\Models\Transaction;
use App\Models\UserLogin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function bvLog(Request $request)
    {

        if ($request->type) {
            if ($request->type == 'leftBV') {
                $data['page_title'] = "Left BV";
                $data['logs'] = BvLog::where('position', 1)->where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'rightBV') {
                $data['page_title'] = "Right BV";
                $data['logs'] = BvLog::where('position', 2)->where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            }elseif ($request->type == 'cutBV') {
                $data['page_title'] = "Cut BV";
                $data['logs'] = BvLog::where('trx_type', '-')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            } else {
                $data['page_title'] = "All Paid BV";
                $data['logs'] = BvLog::where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            }
        }else{
            $data['page_title'] = "BV LOG";
            $data['logs'] = BvLog::orderBy('id', 'desc')->paginate(config('constants.table.default'));
        }

        $data['empty_message'] = 'No data found';
        return view('admin.reports.bvLog', $data);
    }

    public function singleBvLog(Request $request, $id)
    {

        $user = User::findOrFail($id);
        if ($request->type) {
            if ($request->type == 'leftBV') {
                $data['page_title'] = $user->username . " - Left BV";
                $data['logs'] = BvLog::where('user_id', $user->id)->where('position', 1)->where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'rightBV') {
                $data['page_title'] = $user->username . " - Right BV";
                $data['logs'] = BvLog::where('user_id', $user->id)->where('position', 2)->where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'cutBV') {
                $data['page_title'] = $user->username . " - All Cut BV";
                $data['logs'] = BvLog::where('user_id', $user->id)->where('trx_type', '-')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            } else {
                $data['page_title'] = $user->username . " - All Paid BV";
                $data['logs'] = BvLog::where('user_id', $user->id)->where('trx_type', '+')->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));
            }
        }else{
            $data['page_title'] = $user->username . " - All  BV";
            $data['logs'] = BvLog::where('user_id', $user->id)->orderBy('id', 'desc')->with('user')->paginate(config('constants.table.default'));

        }

        $data['empty_message'] = 'No data found';
        return view('admin.reports.bvLog', $data);
    }



    public function refCom(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Referral Commission Logs';
            $transactions = Transaction::where('user_id', $user->id)->where('remark', 'referral_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Referral Commission Logs';
            $transactions = Transaction::where('remark', 'referral_commission')->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }

    public function refComm(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Referral Commission Management Logs';
            $transactions = Transaction::where('user_id', $user->id)->whereIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->where('remark', 'referral_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Referral Commission Management Logs';
            $transactions = Transaction::where('remark', 'referral_commission')->whereIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }
    public function refComu(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Referral Commission User Logs';
            $transactions = Transaction::where('user_id', $user->id)->whereNotIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->where('remark', 'referral_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Referral Commission User Logs';
            $transactions = Transaction::where('remark', 'referral_commission')->whereNotIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }

    public function binary(Request $request)
    {
                // dd($request->all());
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Binary Commission Logs';
            $transactions = Transaction::where('user_id', $user->id)->where('remark', 'binary_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Binary Commission Logs';
            $transactions = Transaction::where('remark', 'binary_commission')->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }
    public function binarym(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Binary Commission Management Logs';
            $transactions = Transaction::where('user_id', $user->id)->whereIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->where('remark', 'binary_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Binary Commission Management Logs';
            $transactions = Transaction::where('remark', 'binary_commission')->whereIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }
    public function binaryu(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Binary Commission User Logs';
            $transactions = Transaction::where('user_id', $user->id)->whereNotIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->where('remark', 'binary_commission')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Binary Commission User Logs';
            $transactions = Transaction::where('remark', 'binary_commission')->whereNotIn('user_id', [153,154,155,156,157,158,159,178,179,181,182,183,184,185,186])->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }
    public function invest(Request $request)
    {
        if ($request->userID)
        {
            $user = User::findOrFail($request->userID);
            $page_title = $user->username . ' - Invest Logs';
            $transactions = Transaction::where('user_id', $user->id)->where('remark', 'purchased_plan')->orwhere('user_id', $user->id)->where('remark', 'purchased_product')->with('user')->latest()->paginate(getPaginate());
        }else {
            $page_title = 'Invest Logs';
            $transactions = Transaction::where('remark', 'purchased_plan')->orwhere('remark', 'purchased_product')->with('user')->latest()->paginate(getPaginate());
        }

        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message'));
    }
    public function transaction(Request $request)
    {
        // dd($request->all());
        if ($request->all()) {
            $search = $request->search;
            $dates  = $request->date;

            if($search != '' && $request->date != ''){
            $date = explode('-',$request->date);

                if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                    $notify[]=['error','Please provide valid date'];
                    return back()->withNotify($notify);
                }
    
                $start = @$date[0];
                $end = @$date[1];
                $transactions = Transaction::whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->with('user')->whereHas('user', function ($user) use ($search) {
                    $user->where('username', 'like',"%$search%")
                    ->orWhere('no_bro', 'like',"%$search%");
                })->orderBy('id','desc')->paginate(getPaginate());
            }elseif($search != '' && $request->date == ''){
                $transactions = Transaction::with('user')->whereHas('user', function ($user) use ($search) {
                    $user->where('username', 'like',"%$search%")
                    ->orWhere('no_bro', 'like',"%$search%");
                })->orderBy('id','desc')->paginate(getPaginate());
            }elseif($search=='' && $request->date != '') {
                $date = explode('-',$request->date);

                if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                    $notify[]=['error','Please provide valid date'];
                    return back()->withNotify($notify);
                }
    
                $start = @$date[0];
                $end = @$date[1];
                $transactions = Transaction::whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->with('user')->orderBy('id','desc')->paginate(getPaginate());
            }else{
                $transactions = Transaction::with('user')->orderBy('id','desc')->paginate(getPaginate());
            }
            $page_title = 'Transactions Search - ' . $search . ' '. $dates??'';
            $empty_message = 'No transactions.';
        }else{
            $transactions = Transaction::with('user')->orderBy('id','desc')->paginate(getPaginate());
            $page_title = 'Transaction Logs';
            $search = '';
            $dates   = '';
        }
        $empty_message = 'No transactions.';
        return view('admin.reports.transactions', compact('page_title', 'transactions', 'empty_message','search','dates'));
    }

    public function transactionSearch(Request $request)
    {
        $request->validate(['search' => 'required']);
        $search = $request->search;
        $date  = $request->date;
        $page_title = 'Transactions Search - ' . $search;
        $empty_message = 'No transactions.';

        $transactions = Transaction::where('created_at',$date)->with('user')->whereHas('user', function ($user) use ($search) {
            $user->where('username', 'like',"%$search%");
            $user->where('no_bro', 'like',"%$search%");
        })->orWhere('trx', $search)->orwhere('details', 'like',"%$search%")->orderBy('id','desc')->paginate(getPaginate());

        return view('admin.reports.transactions', compact('page_title', 'transactions','search', 'empty_message','date'));
    }

    public function loginHistory(Request $request)
    {
        if ($request->search) {
            $search = $request->search;
            $page_title = 'User Login History Search - ' . $search;
            $empty_message = 'No search result found.';
            $login_logs = UserLogin::whereHas('user', function ($query) use ($search) {
                $query->where('username', $search);
            })->orderBy('id','desc')->paginate(getPaginate());
            return view('admin.reports.logins', compact('page_title', 'empty_message', 'search', 'login_logs'));
        }
        $page_title = 'User Login History';
        $empty_message = 'No users login found.';
        $login_logs = UserLogin::orderBy('id','desc')->paginate(getPaginate());
        return view('admin.reports.logins', compact('page_title', 'empty_message', 'login_logs'));
    }

    public function loginIpHistory($ip)
    {
        $page_title = 'Login By - ' . $ip;
        $login_logs = UserLogin::where('user_ip',$ip)->orderBy('id','desc')->paginate(getPaginate());
        $empty_message = 'No users login found.';
        return view('admin.reports.logins', compact('page_title', 'empty_message', 'login_logs'));

    }

    // public function export(Request $request){
    //     // dd($request->all());
    //     $search = $request->search;
    //     $page = $request->page;
    //     if ($search) {
    //         return Excel::download(
    //             new ExportData(
    //             Transaction::query()
    //             ->join('users','users.id','=','transactions.user_id')
    //             ->whereHas('user', function ($user) use ($search) {
    //                 $user->where('username', 'like',"%$search%");
    //             })->orWhere('trx', $search)->orwhere('details', 'like',"%$search%")
    //             ->orderBy('transactions.id','DESC')
    //             ->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
    //         )), 'report.xlsx')
    //         ;
    //     }

    //     if ($page) {
    //         # code...
    //         if ($page == 'Transaction Logs') {
    //             # code...
    //             return Excel::download(
    //                 new ExportData(
    //                 Transaction::query()
    //                 ->join('users','users.id','=','transactions.user_id')
    //                 ->orderBy('transactions.id','DESC')
    //                 ->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
    //             )), 'report.xlsx')
    //             ;
    //         }

    //         if ($page == 'Invest Logs') {
    //             // $remark = "purchased_plan";
    //             return Excel::download(
    //                 new ExportData(
    //                 Transaction::query()
    //                 ->join('users','users.id','=','transactions.user_id')
    //                 ->where('remark', 'purchased_product')
    //                 ->orwhere('remark', 'purchased_plan')
    //                 ->orderBy('transactions.id','DESC')
    //                 ->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
    //             )), 'report.xlsx')
    //             ;
    //         }
    //         if ($page == 'Binary Commission Logs') {
    //             $remark = 'binary_commission';

    //         return Excel::download(
    //             new ExportData(
    //             Transaction::query()
    //             ->join('users','users.id','=','transactions.user_id')
    //             ->where('remark', $remark)
    //             ->orderBy('transactions.id','DESC')
    //             ->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
    //         )), 'report.xlsx')
    //         ;
    //         }


    //     }
    // }


    public function export(Request $request){
        if ($request->all()) {
            $search = $request->search;
            $dates  = $request->date;

            if($search != '' && $request->date != ''){
            $date = explode('-',$request->date);

                if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                    $notify[]=['error','Please provide valid date'];
                    return back()->withNotify($notify);
                }
    
                $start = @$date[0];
                $end = @$date[1];
                $transactions = Transaction::query()->join('users','users.id','=','transactions.user_id')->whereBetween('transactions.created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->with('user')->whereHas('user', function ($user) use ($search) {
                    $user->where('username', 'like',"%$search%")
                    ->orWhere('no_bro', 'like',"%$search%");
                })->orderBy('transactions.id','desc')
                ->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
                );
            }elseif($search != '' && $request->date == ''){
                $transactions = Transaction::query()->join('users','users.id','=','transactions.user_id')->with('user')->whereHas('user', function ($user) use ($search) {
                    $user->where('username', 'like',"%$search%")
                    ->orWhere('no_bro', 'like',"%$search%");
                })->orderBy('transactions.id','desc')->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
            );
            }elseif($search=='' && $request->date != '') {
                $date = explode('-',$request->date);

                if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                    $notify[]=['error','Please provide valid date'];
                    return back()->withNotify($notify);
                }
    
                $start = @$date[0];
                $end = @$date[1];
                $transactions = Transaction::query()->join('users','users.id','=','transactions.user_id')->whereBetween('transactions.created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->with('user')->orderBy('transactions.id','desc')->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
            );
            }else{
                $transactions = Transaction::query()->join('users','users.id','=','transactions.user_id')->with('user')->orderBy('transactions.id','desc')->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
            );
            }
            $page_title = 'Transactions Search - ' . $search . ' '. $dates??'';
            $empty_message = 'No transactions.';
        }else{
            $transactions = Transaction::query()->join('users','users.id','=','transactions.user_id')->with('user')->orderBy('transactions.id','desc')->select(db::raw("DATE_ADD(transactions.created_at, INTERVAL 0 HOUR)"), 'transactions.trx',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username',db::raw("CONCAT(transactions.trx_type,' ',transactions.amount)"), 'transactions.charge','transactions.post_balance','transactions.details'
        );
            $page_title = 'Transaction Logs';
            $search = '';
            $dates   = '';
        }


        return Excel::download(
            new ExportData($transactions), 'report.xlsx');
    }
}
