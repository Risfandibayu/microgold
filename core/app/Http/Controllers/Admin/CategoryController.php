<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    //
    public function index(){
        $page_title = 'Categories';
        $empty_message = 'No Categories found';
        $category = Category::where('status','=',1)->paginate(getPaginate());
        // $products = Product::where('is_custom','!=',1)->paginate(getPaginate());
        return view('admin.category.index', compact('page_title','category', 'empty_message'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name'              => 'required',
        ]);
        
        $category = new Category();
        $category->name             = $request->name;
        $category->status           = 1;
        $category->save();

        adminlog(Auth::guard('admin')->user()->id,'Add Category '.$category->name);

        $notify[] = ['success', 'New Category created successfully'];
        return back()->withNotify($notify);
    }
    public function edit(Request $request){
        $this->validate($request, [
            'name'              => 'required',
            'id'              => 'required',
        ]);
        
        $category = Category::find($request->id);;
        $category->name             = $request->name;
        $category->status           = 1;
        $category->save();

        adminlog(Auth::guard('admin')->user()->id,'Edit Category '.$category->name);

        $notify[] = ['success', 'Category edited successfully'];
        return back()->withNotify($notify);
    }

    public function delete($id){
        $category = Category::find($id);
        $category->status = 0;
        $category->save();

        adminlog(Auth::guard('admin')->user()->id,'Delete Category '.$category->name);
        
        $notify[] = ['success', 'Category Deleted.'];
        return back()->withNotify($notify);
    }
}
