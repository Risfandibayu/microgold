<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function products(){
        $page_title = 'Products';
        $empty_message = 'No Products found';
        $products_resell = Product::where('is_custom','!=',1)->where('is_reseller','=',1)->where('is_special','=',0)->paginate(getPaginate());
        $products_public = Product::where('is_custom','!=',1)->where('is_reseller','=',0)->where('is_special','=',0)->paginate(getPaginate());
        $category = Category::where('status',1)->get();
        // $products = Product::where('is_custom','!=',1)->paginate(getPaginate());
        return view('admin.product.index', compact('page_title','products_resell','products_public', 'empty_message','category'));
    }
    public function productspecial(){
        $page_title = 'Products Special';
        $empty_message = 'No Products found';
        $products = Product::where('is_custom','!=',1)->where('is_special','=',1)->paginate(getPaginate());
        $category = Category::where('status',1)->get();
        // $products = Product::where('is_custom','!=',1)->paginate(getPaginate());
        return view('admin.product.product_special', compact('page_title','products', 'empty_message','category'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function productsStore(Request $request){
        $prod = new Product();
        $prod->name             = $request->name;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/product/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/product/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }

        // dd($request->file('images'));
        $prod->price            = $request->price;
        $prod->weight           = $request->weight;
        $prod->stok             = $request->stok;
        $prod->status           = $request->status?1:0;
        $prod->is_reseller      = $request->reseller?1:0;
        $prod->cat_id           = $request->category;
        $prod->deskripsi        = $request->deskripsi;
        $prod->is_special      = $request->special?1:0;
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,'Add Product '.$prod->name);

        $notify[] = ['success', 'New Plan created successfully'];
        return back()->withNotify($notify);
    }
    public function productsUpdate(Request $request){
        $prod                  = Product::find($request->id);
        $prod->name             = $request->name;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/product/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/product/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }

        // dd($request->file('images'));
        $prod->price            = $request->price;
        $prod->weight           = $request->weight;
        // $prod->stok           = $request->stok;
        $prod->status           = $request->status?1:0;
        $prod->is_reseller      = $request->reseller?1:0;
        $prod->cat_id           = $request->category;
        $prod->deskripsi        = $request->deskripsi;
        $prod->is_special      = $request->special?1:0;
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,'Update Product '.$prod->name);

        $notify[] = ['success', 'Product edited successfully'];
        return back()->withNotify($notify);
    }

    public function upStok(Request $request){
        $prod  = Product::find($request->id);
        $prod->stok           = $request->stok;
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,'Update Stock '.$prod->stok.' to Product '.$prod->name);

        $notify[] = ['success', 'Update Stock successfully'];
        return back()->withNotify($notify);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
