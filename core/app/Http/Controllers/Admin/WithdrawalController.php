<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportData;
use App\Models\GeneralSetting;
use App\Http\Controllers\Controller;
use App\Models\rekening;
use App\Models\Transaction;
use App\Models\User;
use App\Models\WithdrawMethod;
use App\Models\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    public function pending()
    {
        $page_title = 'Pending Withdrawals';
        $withdrawals = Withdrawal::where('status', 2)->with(['user','method'])->latest()->paginate(getPaginate());
        $empty_message = 'No withdrawal is pending';
        $type = 'pending';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message','type'));
    }
    public function approved()
    {
        $page_title = 'Approved Withdrawals';
        $withdrawals = Withdrawal::where('status', 1)->with(['user','method'])->latest()->paginate(getPaginate());
        $empty_message = 'No withdrawal is approved';
        $type = 'approved';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message','type'));
    }

    public function rejected()
    {
        $page_title = 'Rejected Withdrawals';
        $withdrawals = Withdrawal::where('status', 3)->with(['user','method'])->latest()->paginate(getPaginate());
        $empty_message = 'No withdrawal is rejected';
        $type = 'rejected';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message','type'));
    }

    public function log()
    {
        $page_title = 'Withdrawals Log';
        $withdrawals = Withdrawal::where('status', '!=', 0)->with(['user','method'])->latest()->paginate(getPaginate());
        $empty_message = 'No withdrawal history';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message'));
    }


    public function logViaMethod($method_id,$type = null){
        $method = WithdrawMethod::findOrFail($method_id);
        if ($type == 'approved') {
            $page_title = 'Approved Withdrawal Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 1)->with(['user','method'])->latest()->paginate(getPaginate());
        }elseif($type == 'rejected'){
            $page_title = 'Rejected Withdrawals Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 3)->with(['user','method'])->latest()->paginate(getPaginate());

        }elseif($type == 'pending'){
            $page_title = 'Pending Withdrawals Via '.$method->name;
            $withdrawals = Withdrawal::where('status', 2)->with(['user','method'])->latest()->paginate(getPaginate());
        }else{
            $page_title = 'Withdrawals Via '.$method->name;
            $withdrawals = Withdrawal::where('status', '!=', 0)->with(['user','method'])->latest()->paginate(getPaginate());
        }
        $empty_message = 'Withdraw Log Not Found';
        return view('admin.withdraw.withdrawals', compact('page_title', 'withdrawals', 'empty_message','method'));
    }


    public function search(Request $request, $scope)
    {
        $search = $request->search;
        $page_title = '';
        $empty_message = 'No search result found.';

        $withdrawals = Withdrawal::with(['user', 'method'])->where('status','!=',0)->where(function ($q) use ($search) {
            $q->where('trx', 'like',"%$search%")
                ->orWhereHas('user', function ($user) use ($search) {
                    $user->where('username', 'like',"%$search%");
                })
                ->orWhereHas('user', function ($user) use ($search) {
                    $user->where('trx_id', 'like',"%$search%");
                });
        });

        switch ($scope) {
            case 'pending':
                $page_title .= 'Pending Withdrawal Search';
                $withdrawals = $withdrawals->where('status', 2);
                break;
            case 'approved':
                $page_title .= 'Approved Withdrawal Search';
                $withdrawals = $withdrawals->where('status', 1);
                break;
            case 'rejected':
                $page_title .= 'Rejected Withdrawal Search';
                $withdrawals = $withdrawals->where('status', 3);
                break;
            case 'log':
                $page_title .= 'Withdrawal History Search';
                break;
        }

        $withdrawals = $withdrawals->paginate(getPaginate());
        $page_title .= ' - ' . $search;

        return view('admin.withdraw.withdrawals', compact('page_title', 'empty_message', 'search', 'scope', 'withdrawals'));
    }

    public function dateSearch(Request $request,$scope){
        $search = $request->date;
        if (!$search) {
            return back();
        }
        $date = explode('-',$search);

        if(!(@strtotime($date[0]) && @strtotime($date[1]))){
            $notify[]=['error','Please provide valid date'];
            return back()->withNotify($notify);
        }

        $start = @$date[0];
        $end = @$date[1];
        // if ($start) {
        //     $withdrawals = Withdrawal::where('status','!=',0)->where('created_at','>',Carbon::parse($start)->subDays(1))->where('created_at','<=',Carbon::parse($start)->addDays(1));
        // }
        // if($end){
        //     $withdrawals = Withdrawal::where('status','!=',0)->where('created_at','>',Carbon::parse($start)->subDays(1))->where('created_at','<',Carbon::parse($end));
        // }
        $withdrawals = Withdrawal::where('status','!=',0)->whereBetween('created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)]);
        if ($request->method) {
            $method = WithdrawMethod::findOrFail($request->method);
            $withdrawals = $withdrawals->where('method_id',$method->id);
        }

        switch ($scope) {
            case 'pending':
                $withdrawals = $withdrawals->where('status', 2);
                break;
            case 'approved':
                $withdrawals = $withdrawals->where('status', 1);
                break;
            case 'rejected':
                $withdrawals = $withdrawals->where('status', 3);
                break;
        }

        $withdrawals = $withdrawals->with(['user', 'method'])->paginate(getPaginate());
        $page_title = 'Withdraw Log';
        $empty_message = 'No Withdrawals Found';
        $dateSearch = $search;
        return view('admin.withdraw.withdrawals', compact('page_title', 'empty_message', 'dateSearch', 'withdrawals','scope'));


    }

    public function details($id)
    {
        $general = GeneralSetting::first();
        $withdrawal = Withdrawal::where('id',$id)->where('status', '!=', 0)->with(['user','method'])->firstOrFail();
        $page_title = $withdrawal->user->username.' Withdraw Requested ' . getAmount($withdrawal->amount) . ' '.$general->cur_text;
        $details = ($withdrawal->withdraw_information != null) ? json_encode($withdrawal->withdraw_information) : null;



        $methodImage =  getImage(imagePath()['withdraw']['method']['path'].'/'. $withdrawal->method->image,'800x800');

        return view('admin.withdraw.detail', compact('page_title', 'withdrawal','details','methodImage'));
    }

    public function approve(Request $request)
    {
        $request->validate(['id' => 'required|integer']);
        $withdraw = Withdrawal::where('id',$request->id)->where('status',2)->with('user')->firstOrFail();
        $withdraw->status = 1;
        $withdraw->admin_feedback = $request->details;
        // $withdraw->save();

        $general = GeneralSetting::first();
        // notify($withdraw->user, 'WITHDRAW_APPROVE', [
        //     'method_name' => $withdraw->method->name,
        //     'method_currency' => $withdraw->currency,
        //     'method_amount' => getAmount($withdraw->final_amount),
        //     'amount' => getAmount($withdraw->amount),
        //     'charge' => getAmount($withdraw->charge),
        //     'currency' => $general->cur_text,
        //     'rate' => getAmount($withdraw->rate),
        //     'trx' => $withdraw->trx,
        //     'admin_details' => $request->details
        // ]);

        // adminlog(Auth::guard('admin')->user()->id,'Approve Withdraw '.$withdraw->trx .' With Detail '.$withdraw->admin_feedback);

        $user = user::where('id',$withdraw->user_id)->first();
        $userBank = rekening::where('user_id',$user->id)->first();

        $va           = env('IPAY_VA'); //get on iPaymu dashboard
        $secret       = env('IPAY_SECRET'); //get on iPaymu dashboard

        if (env('APP_ENV') != 'production') {
            # code...
            $url          = 'https://sandbox.ipaymu.com/api/v2/withdrawbank'; // for development mode
        }else{
            $url          = 'https://my.ipaymu.com/api/v2/withdrawbank'; // for production mode
        }
        
        $method       = 'POST'; //method
        
        //Request Body//
        $body['bankCode']    = $userBank->nama_bank;
        $body['bankName']    = bank($userBank->nama_bank);
        $body['bankNumber']    = $userBank->no_rek;
        $body['bankAccount']    = $userBank->nama_akun;
        $body['amount']    = $withdraw->amount - $withdraw->charge;
        $body['notes']    = 'Withdrawal Microgold';
        $body['referenceId'] = $withdraw->trx;
        
        // dd($body);

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature


        $ch = curl_init($url);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'va: ' . $va,
            'signature: ' . $signature,
            'timestamp: ' . $timestamp
        );

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, count($body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $err = curl_error($ch);
        $ret = curl_exec($ch);
        curl_close($ch);

        


        // $withdraw->status = 2;
        // $withdraw->status = 1;
        // $withdraw->save();
        // $user->balance  -=  $withdraw->amount;
        // $user->save();



        $transaction = new Transaction();
        $transaction->user_id = $withdraw->user_id;
        $transaction->amount = getAmount($withdraw->amount);
        $transaction->post_balance = getAmount($user->balance);
        $transaction->charge = getAmount($withdraw->charge);
        $transaction->trx_type = '-';
        $transaction->details = getAmount($withdraw->final_amount) . ' ' . $withdraw->currency . ' Withdraw Via ' . $withdraw->method->name;
        $transaction->trx =  $withdraw->trx;
        // $transaction->save();

        // $adminNotification = new AdminNotification();
        // $adminNotification->user_id = $user->id;
        // $adminNotification->title = 'New withdraw request from '.$user->username;
        // $adminNotification->click_url = route('admin.withdraw.details',$withdraw->id);
        // $adminNotification->save();

        // notify($user, 'WITHDRAW_REQUEST', [
        //     'method_name' => $withdraw->method->name,
        //     'method_currency' => $withdraw->currency,
        //     'method_amount' => getAmount($withdraw->final_amount),
        //     'amount' => getAmount($withdraw->amount),
        //     'charge' => getAmount($withdraw->charge),
        //     'currency' => $general->cur_text,
        //     'rate' => getAmount($withdraw->rate),
        //     'trx' => $withdraw->trx,
        //     'post_balance' => getAmount($user->balance),
        //     'delay' => $withdraw->method->delay
        // ]);

        if($err) {
            echo $err;
        } else {

            //Response
            $ret = json_decode($ret);
            // dd($ret);
            if($ret->Status == 200) {
                // header('Location:' . $url);
                // dd($url);
                $withdraw->trx_id = $ret->Data->TransactionId;
                $withdraw->save();
                $user->save();
                $transaction->save();
                // $adminNotification->save();
                notify($user, 'WITHDRAW_REQUEST', [
                    'method_name' => $withdraw->method->name,
                    'method_currency' => $withdraw->currency,
                    'method_amount' => getAmount($withdraw->final_amount),
                    'amount' => getAmount($withdraw->amount),
                    'charge' => getAmount($withdraw->charge),
                    'currency' => $general->cur_text,
                    'rate' => getAmount($withdraw->rate),
                    'trx' => $withdraw->trx,
                    'post_balance' => getAmount($user->balance),
                    'delay' => $withdraw->method->delay
                ]);

                adminlog(Auth::guard('admin')->user()->id,'Approve Withdraw '.$withdraw->trx .' With Detail '.$withdraw->admin_feedback);

                $notify[] = ['success', 'Withdrawal Marked  as Approved.'];
                return redirect()->route('admin.withdraw.pending')->withNotify($notify);
            } else {
                // echo $ret;
                $notify[] = ['error', 'Invalid Withdrawal, Please check bank account and make sure it`s correct.'];
                return back()->withNotify($notify);
            }
            //End Response
        }

        // $notify[] = ['success', 'Withdrawal Marked  as Approved.'];
        // return redirect()->route('admin.withdraw.pending')->withNotify($notify);
    }


    public function reject(Request $request)
    {
        $general = GeneralSetting::first();
        $request->validate(['id' => 'required|integer']);
        $withdraw = Withdrawal::where('id',$request->id)->where('status',2)->firstOrFail();

        $withdraw->status = 3;
        $withdraw->admin_feedback = $request->details;
        $withdraw->save();

        $user = User::find($withdraw->user_id);
        $user->balance += getAmount($withdraw->amount);
        $user->save();



            $transaction = new Transaction();
            $transaction->user_id = $withdraw->user_id;
            $transaction->amount = $withdraw->amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = 0;
            $transaction->trx_type = '+';
            $transaction->details = getAmount($withdraw->amount) . ' ' . $general->cur_text . ' Refunded from Withdrawal Rejection';
            $transaction->trx = $withdraw->trx;
            $transaction->save();




        notify($user, 'WITHDRAW_REJECT', [
            'method_name' => $withdraw->method->name,
            'method_currency' => $withdraw->currency,
            'method_amount' => getAmount($withdraw->final_amount),
            'amount' => getAmount($withdraw->amount),
            'charge' => getAmount($withdraw->charge),
            'currency' => $general->cur_text,
            'rate' => getAmount($withdraw->rate),
            'trx' => $withdraw->trx,
            'post_balance' => getAmount($user->balance),
            'admin_details' => $request->details
        ]);

        adminlog(Auth::guard('admin')->user()->id,'Reject Withdraw '.$withdraw->trx.' With Detail '.$withdraw->admin_feedback);

        $notify[] = ['success', 'Withdrawal has been rejected.'];
        return redirect()->route('admin.withdraw.pending')->withNotify($notify);
    }

    public function export(Request $request){
        // dd($request->all());
        $scope = $request->page;
        $pt = $request->paget;
        $search = $request->search;
        $dates = $request->date;
        $status = $request->status;
        $code = '';

        $date = explode('-',$dates);
        $start  = @$date[0];
        $end    = @$date[1];

        if($request->search){
            switch ($scope) {
                case 'pending':
                    $code = 2;
                    break;
                case 'approved':
                    $code = 1;
                    break;
                case 'rejected':
                    $code = 3;
                    break;
                case 'successful':
                    $code = 1;
                    break;
                default:
                    return Excel::download(
                        new ExportData(
                        Withdrawal::query()
                        ->where('withdrawals.status','!=',0)
                        ->where('withdrawals.trx', 'like', "%$search%")
                        ->orWhere('users.username', 'like', "%$search%")
                        ->where('withdrawals.status','!=',0)
                        ->join('users','users.id','=','withdrawals.user_id')
                        ->orderBy('withdrawals.id','DESC')
                        ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))"))), 'withdrawal1.xlsx');
                
            }
            return Excel::download(
                new ExportData(
                Withdrawal::query()
                ->where('withdrawals.status','!=',0)
                ->where('withdrawals.status','=',$code)
                ->where('withdrawals.trx', 'like', "%$search%")
                ->orWhere('users.username', 'like', "%$search%")
                ->where('withdrawals.status','!=',0)
                ->where('withdrawals.status','=',$code)
                ->join('users','users.id','=','withdrawals.user_id')
                ->orderBy('withdrawals.id','DESC')
                ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))"))), 'withdrawal2.xlsx');
        }

        if ($request->date) {
            # code...
             switch ($scope) {
                case 'pending':
                    $code = 2;
                    break;
                case 'approved':
                    $code = 1;
                    break;
                case 'successful':
                    $code = 1;
                    break;
                case 'rejected':
                    $code = 3;
                    break;
                case 'list':
                    return Excel::download(
                        new ExportData(
                        Withdrawal::query()
                        ->where('withdrawals.status','!=',0)
                        ->whereBetween('withdrawals.created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])
                        ->join('users','users.id','=','withdrawals.user_id')
                        ->orderBy('withdrawals.id','DESC')
                        ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))"))), 'withdrawal.xlsx');
                    break;
            }
            
            
                if ($code) {
                    # code...
                return Excel::download(
                    new ExportData(
                    Withdrawal::query()
                    ->where('withdrawals.status','=',$code)
                    ->whereBetween('withdrawals.created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])
                    ->join('users','users.id','=','withdrawals.user_id')
                    ->orderBy('withdrawals.id','DESC')
                    ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))")
                    )), 'withdrawal.xlsx')
                    ;
                }
                return Excel::download(
                    new ExportData(
                    Withdrawal::query()
                    ->where('withdrawals.status','!=',0)
                    ->whereBetween('withdrawals.created_at', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])
                    // ->where('withdrawals.status','=',$code)
                    ->join('users','users.id','=','withdrawals.user_id')
                    ->orderBy('withdrawals.id','DESC')
                    ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))"))), 'withdrawal.xlsx');
        }

        switch ($pt) {
            case 'Pending Withdrawals':
                $code = 2;
                break;
            case 'Approved Withdrawals':
                $code = 1;
                break;
            case 'Successful Withdrawals':
                $code = 1;
                break;
            case 'Rejected Withdrawals':
                $code = 3;
                break;
            case 'Withdrawals Log':
                return Excel::download(
                    new ExportData(
                    Withdrawal::query()
                    ->where('withdrawals.status','!=',0)
                    ->join('users','users.id','=','withdrawals.user_id')
                    ->orderBy('withdrawals.id','DESC')
                    ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))"))), 'withdrawal.xlsx');
            break;
        }

        if ($request->status) {
            $code = $status;
        }

        if ($code) {
            # code...
        return Excel::download(
            new ExportData(
            Withdrawal::query()
            ->where('withdrawals.status','=',$code)
            ->join('users','users.id','=','withdrawals.user_id')
            ->orderBy('withdrawals.id','DESC')
            ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))")
            )), 'withdrawal.xlsx')
            ;
        }
        
        return Excel::download(
            new ExportData(
            Withdrawal::query()
            ->where('withdrawals.status','!=',0)
            ->join('users','users.id','=','withdrawals.user_id')
            ->orderBy('withdrawals.id','DESC')
            ->select(db::raw("DATE_ADD(withdrawals.created_at, INTERVAL 0 HOUR)"),'withdrawals.trx','withdrawals.trx_id',db::raw("CONCAT(users.firstname, ' ',users.lastname ) AS nama"),'users.username','users.email','withdrawals.amount',db::raw("if(withdrawals.status = 2, 'pending',if(withdrawals.status = 1,'approved','Rejected'))")
            )), 'withdrawal.xlsx')
            ;

    }
}
