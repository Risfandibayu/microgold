<?php

namespace App\Http\Controllers;

use App\Models\alamat;
use App\Models\BvLog;
use App\Models\GeneralSetting;
use App\Models\Gold;
use App\Models\Plan;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserExtra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{

    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }

    function planIndex()
    {
        $data['page_title'] = "Packages";
        $data['plans'] = Plan::whereStatus(1)->get();
        $data['alamat'] = alamat::where('user_id','=',Auth::user()->id)->get();
        
        return view($this->activeTemplate . '.user.plan', $data);

    }

    // function planStore(Request $request){
    //     brodev(Auth::user()->id, $request->qty);
    // }

    function planStore(Request $request)
    {
        // dd($request->all());
        // dd(date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")));
        // dd(now() < date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")) ? 'ok' : 'no');
        $plan = Plan::where('id', $request->plan_id)->where('status', 1)->firstOrFail();

        
        if ($plan->id == 1) {
            
            if (!$request->shipmethod) {
                # code...
                $notify[] = ['error', 'Please select shipping method first'];
                return back()->withNotify($notify);
            }

            if ($request->shipmethod == 1) {
                # code...
                if (!$request->pickdate) {
                    # code...
                    $notify[] = ['error', 'Please enter Bro Pack pick up date'];
                    return back()->withNotify($notify);
                }
                if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                    # code...
                    $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                    return back()->withNotify($notify);
                }
            }else{
                if (!$request->alamat) {
                    # code...
                    $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting')->withNotify($notify);
                }
                
                $alamat = alamat::where('id', $request->alamat)->first();
                if (!$alamat->kode_pos) {
                    # code...
                    $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                    return redirect()->back()->withNotify($notify);
                }
            }
        }

        $this->validate($request, [
            'plan_id' => 'required|integer',
            'referral' => 'required',
            'position' => 'required',
            // 'alamat' => 'required',
        ]);
        // dd($request->all());
        
        $gnl = GeneralSetting::first();
        // dd(date('Y-m-d,H:i:s'));

        $brolimit = user::where('plan_id','!=',0)->count();
        // dd($brolimit);
        if (date('Y-m-d,H:i:s') > '2022-09-02,23:59:59') {
            # code...
            // dd('s');
            $g1 = 15;
            $g2 = 10;
            $g3 = 31;
            $g4 = 0;
            $tot = 56;
        }else{
            // dd('w');
            $g1 = 70;
            $g2 = 20;
            $g3 = 7;
            $g4 = 2;
            $tot = 99;
        }


        $user = User::find(Auth::id());
        $ref_user = User::where('no_bro', $request->referral)->first();
        $up_user = User::where('no_bro', $request->upline)->first();
        if (!$up_user && $request->upline) {
            $notify[] = ['error', 'Invalid Direct BRO Number.'];
            return back()->withNotify($notify);
        }

        if ($up_user) {
            # code...
            $cek_pos = User::where('pos_id', $up_user->id)->where('position',$request->position)->first();
    
            if(!treeFilter($up_user->id,$ref_user->id)){
                $notify[] = ['error', 'Refferal and Direct BRO number not in the same tree.'];
                return back()->withNotify($notify);
            }
            
            if ($cek_pos) {
                $notify[] = ['error', 'Node you input is already filled.'];
                return back()->withNotify($notify);
            }
        }

        // if ($up_user == null) {
        //     $notify[] = ['error', 'Invalid Upline BRO Number.'];
        //     return back()->withNotify($notify);
        // }

        if ($ref_user == null) {
            $notify[] = ['error', 'Invalid Refferal BRO Number.'];
            return back()->withNotify($notify);
        }
        

        if($ref_user->no_bro == $user->no_bro){
            $notify[] = ['error', 'Invalid Input BRO Number. You can`t input your own BRO number.'];
            return back()->withNotify($notify);
        }


        if ($user->balance < ($plan->price * $request->qty)) {
            $notify[] = ['error', 'Insufficient Balance'];
            return back()->withNotify($notify);
        }

        // dd($request->all());

        DB::beginTransaction();

        try {
            $oldPlan = $user->plan_id;
            
            $pos = getPosition($ref_user->id, $request->position);
            $user->no_bro       = generateUniqueNoBro();
            $user->ref_id= $ref_user->id;
            // $user->pos_id= $pos['pos_id'];
            // $user->pos_id= $up_user->id;
            $user->pos_id= isset($up_user->id) ? $up_user->id : $pos['pos_id'];
            $user->position= $request->position;
            $user->plan_id = $plan->id;
            $user->balance -= ($plan->price * $request->qty);
            $user->total_invest += ($plan->price * $request->qty);
            $user->bro_qty += ($request->qty - 1);
            $user->is_matriks = $request->qty>14 ? 1 : 0;
            $user->save();

            if ($plan->id == 1) {
                # code...

            $gold = Gold::where('user_id',Auth::user()->id)->first();
            $gold1 = Gold::where('user_id',Auth::user()->id)->where('prod_id',1)->first();
            $gold2 = Gold::where('user_id',Auth::user()->id)->where('prod_id',2)->first();
            $gold3 = Gold::where('user_id',Auth::user()->id)->where('prod_id',3)->first();
            $gold4 = Gold::where('user_id',Auth::user()->id)->where('prod_id',4)->first();

            if($gold){
                if($gold1){
                    $gold1->qty += $g1 * $request->qty;
                    $gold1->save();
                }else{
                    $newg = new Gold();
                    $newg->user_id = Auth::user()->id;
                    $newg->prod_id = 1;
                    $newg->qty = $g1 * $request->qty;
                    $newg->save();
                }

                if($gold2){
                    $gold2->qty += $g2 * $request->qty;
                    $gold2->save();
                }else{
                    $newg = new Gold();
                    $newg->user_id = Auth::user()->id;
                    $newg->prod_id = 2;
                    $newg->qty = $g2 * $request->qty;
                    $newg->save();
                }

                if($gold3){
                    $gold3->qty += $g3 * $request->qty;
                    $gold3->save();
                }else{
                    $newg = new Gold();
                    $newg->user_id = Auth::user()->id;
                    $newg->prod_id = 3;
                    $newg->qty = $g3 * $request->qty;
                    $newg->save();
                }

                if($gold4){
                    $gold4->qty += $g4 * $request->qty;
                    $gold4->save();
                }else{
                    $newg = new Gold();
                    $newg->user_id = Auth::user()->id;
                    $newg->prod_id = 4;
                    $newg->qty = $g4 * $request->qty;
                    $newg->save();
                }


            }else{
                $newg = new Gold();
                $newg->user_id = Auth::user()->id;
                $newg->prod_id = 1;
                $newg->qty = $g1 * $request->qty;
                $newg->save();

                $newg = new Gold();
                $newg->user_id = Auth::user()->id;
                $newg->prod_id = 2;
                $newg->qty = $g2 * $request->qty;
                $newg->save();

                $newg = new Gold();
                $newg->user_id = Auth::user()->id;
                $newg->prod_id = 3;
                $newg->qty = $g3 * $request->qty;
                $newg->save();

                $newg = new Gold();
                $newg->user_id = Auth::user()->id;
                $newg->prod_id = 4;
                $newg->qty = $g4 * $request->qty;
                $newg->save();
            }

            if ($request->shipmethod == 1) {
                # code...
                brodevpick(Auth::user()->id, $request->qty,date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")));
            }else{
                brodev(Auth::user()->id, $request->qty,$request->alamat);
            }
            }

            if ($plan->id == 1) {
                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $request->qty,
                    'trx_type' => '-',
                    'details' => 'Purchased ' . $plan->name . ' For '.$request->qty.' BRO and Get '.$request->qty * $tot .' Pieces Gold',
                    'remark' => 'purchased_plan',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);

                // dd($user);

                sendEmail2($user->id, 'plan_purchased', [
                    'plan' => $plan->name. ' For '.$request->qty.' BRO and Get '.$request->qty * $tot .' Pieces Gold',
                    'amount' => getAmount($plan->price * $request->qty),
                    'currency' => $gnl->cur_text,
                    'trx' => $trx->trx,
                    'post_balance' => getAmount($user->balance),
                ]);
            }else{
                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $request->qty,
                    'trx_type' => '-',
                    'details' => 'Purchased ' . $plan->name . ' For '.$request->qty.' BRO',
                    'remark' => 'purchased_plan',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);
    
                // dd($user);
    
                sendEmail2($user->id, 'plan_purchased', [
                    'plan' => $plan->name. ' For '.$request->qty.' BRO',
                    'amount' => getAmount($plan->price * $request->qty),
                    'currency' => $gnl->cur_text,
                    'trx' => $trx->trx,
                    'post_balance' => getAmount($user->balance),
                ]);
            }


            if ($oldPlan == 0) {
                updatePaidCount2($user->id);
            }
            $details = Auth::user()->username . ' Subscribed to ' . $plan->name . ' plan.';

            // updateBV($user->id, $plan->bv, $details);

            // if ($plan->tree_com > 0) {
            //     treeComission($user->id, $plan->tree_com, $details);
            // }

            referralCommission2($user->id, $details, $request->qty);
            DB::commit();

            $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
            return redirect()->route('user.home')->withNotify($notify);

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                $notify[] = ['error', 'Purchase failed, please try again.'.$e];
                return redirect()->back()->withNotify($notify);
            }

    }


    public function binaryCom()
    {
        $data['page_title'] = "Binary Commission";
        $data['logs'] = Transaction::where('user_id', auth()->id())->where('remark', 'binary_commission')->orderBy('id', 'DESC')->paginate(config('constants.table.default'));
        $data['empty_message'] = 'No data found';
        return view($this->activeTemplate . '.user.transactions', $data);
    }

    public function binarySummery()
    {
        $data['page_title'] = "Binary Summery";
        $data['logs'] = UserExtra::where('user_id', auth()->id())->firstOrFail();
        return view($this->activeTemplate . '.user.binarySummery', $data);
    }

    public function bvlog(Request $request)
    {

        if ($request->type) {
            if ($request->type == 'leftBV') {
                $data['page_title'] = "Left BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('position', 1)->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'rightBV') {
                $data['page_title'] = "Right BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('position', 2)->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'cutBV') {
                $data['page_title'] = "Cut BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('trx_type', '-')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } else {
                $data['page_title'] = "All Paid BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            }
        } else {
            $data['page_title'] = "BV LOG";
            $data['logs'] = BvLog::where('user_id', auth()->id())->orderBy('id', 'desc')->paginate(config('constants.table.default'));
        }

        $data['empty_message'] = 'No data found';
        return view($this->activeTemplate . '.user.bvLog', $data);
    }

    public function myRefLog()
    {
        $data['page_title'] = "My Referral";
        $data['empty_message'] = 'No data found';
        $data['logs'] = User::where('ref_id', auth()->id())
        ->join('transactions','transactions.user_id','users.id')
        ->where('transactions.remark','purchased_plan')
        ->select('users.*','transactions.created_at as ct')
        ->orderBy('transactions.created_at','DESC')->paginate(config('constants.table.default'));
        return view($this->activeTemplate . '.user.myRef', $data);
    }

    public function myTree()
    {
        $data['tree'] = showTreePage(Auth::id());
        $data['page_title'] = "My Group Sales";
        return view($this->activeTemplate . 'user.myTree', $data);
    }


    public function otherTree(Request $request, $username = null)
    {
        if ($request->username) {
            $user = User::where('username', $request->username)->first();
        } else {
            $user = User::where('username', $username)->first();
        }
        if ($user && treeAuth($user->id, auth()->id())) {
            $data['tree'] = showTreePage($user->id);
            $data['page_title'] = "Tree of " . $user->fullname;
            return view($this->activeTemplate . 'user.myTree', $data);
        }

        $notify[] = ['error', 'Tree Not Found or You do not have Permission to view that!!'];
        return redirect()->route('user.my.tree')->withNotify($notify);

    }

    public function buyBROStore(Request $request){
        // dd($request->all());
        $this->validate($request, [
            'qtyy' => 'required|integer|min:1',
        ]);
        $user = User::find(Auth::id());
        $plan = Plan::where('id', $request->plan_id)->where('status', 1)->firstOrFail();
        $gnl = GeneralSetting::first();
        if ($user->balance < ($plan->price * $request->qtyy)) {
            $notify[] = ['error', 'Insufficient Balance'];
            return back()->withNotify($notify);
        }
        $user->balance -= ($plan->price * $request->qtyy);
        $user->total_invest += ($plan->price * $request->qtyy);
        $user->bro_qty += $request->qtyy;
        $user->save();

        $trx = $user->transactions()->create([
            'amount' => $plan->price * $request->qtyy,
            'trx_type' => '-',
            'details' => 'Purchased new BRO quantity for '.$request->qtyy.' BRO',
            'remark' => 'purchased_plan',
            'trx' => getTrx(),
            'post_balance' => getAmount($user->balance),
        ]);

        $notify[] = ['success', 'Purchased new BRO quantity for '.$request->qtyy.' BRO Successfully'];
        return redirect()->route('user.home')->withNotify($notify);

    }

    public function refJoin(Request $request){
        $u = User::where('no_bro',$request->ref)->first();
        if(!$u){
            $notify[] = ['error', 'BRO Refferal number not found!'];
            return redirect()->back()->withNotify($notify);
        }
        if(Auth::user()->plan_id != 0){
            $notify[] = ['error', 'You already purchase plan!'];
            return redirect()->back()->withNotify($notify);
        }
        if(Auth::user()->no_bro == $request->ref){
            $notify[] = ['error', 'You can`t use your own bro number'];
            return redirect()->back()->withNotify($notify);
        }
        // dd($request->all());
        // $data['tree'] = showTreePage(Auth::id());
        $data['plan'] = Plan::where('status','!=',0)->get();
        $data['ref'] = $request->ref;
        $data['position'] = $request->position;
        $data['alamat'] = alamat::where('user_id','=',Auth::user()->id)->get();
        $data['page_title'] = "Purchase Plan By Refferal";
        return view($this->activeTemplate . '.user.joinref', $data);
    }

    public function planApi(Request $request){
        $validator = validator($request->all(), [
            'email' => 'required',
            'bro' => 'required',
        ]);

        $user = User::where('email',$request->email)->first();
        if (!$user) {
            return response()->json(['status'=> 'fail','errors' => 'The user not found'], 422);
        }

        $user->no_bro =  $request->bro;
        $user->plan_id =  1;
        $user->save();
        return response()->json(['status'=> 'success','message' => 'Update User successfully']);
    }


}
