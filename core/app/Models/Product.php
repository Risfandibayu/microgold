<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public function cord()
    {
        return $this->belongsTo(corder::class);
    }
    public function cat()
    {
        return $this->hasOne(Category::class,'id','cat_id');
    }
}
