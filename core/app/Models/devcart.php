<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class devcart extends Model
{
    use HasFactory;

    public function gold()
    {
        return $this->belongsTo(Gold::class);
    }
}
