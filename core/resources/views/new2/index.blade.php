<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>MicroGold | Indonesia Emas 2045</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('assets/new2')}}/img/logo-MG2.png" rel="icon">
  <link href="{{asset('assets/new2')}}/img/logo-MG2.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!-- <link
    href="https://fonts.googleapis.com/css2?family=DM+Serif+Display:ital@0;1&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet"> -->

  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/new2')}}/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/new2')}}/css/style.css" rel="stylesheet">
  <link href="{{asset('assets/new2')}}/css/product.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />




  <!-- =======================================================
  * Template Name: Arsha
  * Updated: Sep 18 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>


  </style>
</head>

<body>

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-start">
      <!-- Brand Logo -->

      <div class="col-11 col-md-2">
        <!-- <h1 class="logo spase-nav"><a href="index.html">Arsha</a></h1> -->
        <a href="" class="logo spase-nav"><img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="" class=""></a>
      </div>
      <div class="col-1 col-md-8">


        <!-- Navbar Menu -->
        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
            <li><a class="nav-link scrollto" href="#about">Tentang</a></li>
            <li><a class="nav-link scrollto" href="#product">Produk</a></li>
            <li><a class="nav-link scrollto" href="#faq">FAQ</a></li>
            <li><a class="nav-link scrollto" href="https://wa.me/+6281282174631" target="_blank">Kontak Kami</a></li>
            <hr>
            @if (!Auth::check())
            <div class="text-center b-lr-m justify-content-center">
              <a class="btn btn-block btn-register" style="justify-content: center;color: white;"
                href="{{route('user.register')}}">Register</a>
              <a class="btn btn-block btn-login" style="justify-content: center;" href="{{route('user.login')}}">Login</a>
            </div>
            @else
            <div class="text-center b-lr-m justify-content-center">
            <a class="btn btn-block btn-register" style="justify-content: center;color: white;" href="{{url('/user/dashboard')}}">Dashboard</a>
            </div>
            @endif
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
      </div>
      <div class="col-md-3 b-lr row">
        @if (!Auth::check())
            <div class="col-6 row">
            <a class="btn btn-block btn-login" href="{{route('user.login')}}">Login</a>
            </div>
            <div class="col-6 row">
            <a class="btn btn-block btn-register " href="{{route('user.register')}}">Register</a>
            </div>
        @else
            <div class="col-12 row">
            <a class="btn btn-block btn-register" href="{{url('/user/dashboard')}}">Dashboard</a>
            </div>
            @endif
      </div>
    </div>
  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-1" data-aos="fade-up" data-aos-delay="200">
          <span class="indoemas badge badge-pill badge-secondary col-lg-4">Indonesia emas 2045</span>
          <h1 class="mb-4">Semua Orang Bisa Punya Emas</h1>
          <h3 class="mb-4">Microgold menyediakan emas bersertifikat dengan ukuran <br> 0.01 gram, 0,02 gram, dan 0,05 gram.</h3>
          <div class="d-flex justify-content-center justify-content-lg-start mb-4 row col-md-8">
            <div class="col-6">
              <a href="#product" class="btn-lihat-produk scrollto">Lihat Produk</a>
            </div>
            <div class="col-6">
              <a href="{{route('user.register')}}" class="btn-get-started scrollto">Register</a>
            </div>
          </div>
          <h2>Anda bisa tukarkan dengan emas Logam mulia bersertifikat ANTAM.</h2>
        </div>
        <div class="col-lg-6 order-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="{{asset('assets/new2')}}/img/Hero Image.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <section id="about" class="services" style="background-color: #1B1B1B;">
      <div class="container" data-aos="fade-up">

        <div class="section-title row justify-content-center">
          <h2 class="mb-2">Pentingnya Emas Untuk Masa Depan</h2>
          <div class="col-md-8">
            <p>Emas merupakan logam mulai dengan karakteristik yang stabil, tidak berubah zat, tidak beroksidasi di udara normal, dan mengandung unsur murni. Emas biasanya digunakan sebagai koleksi.</p>
          </div>
        </div>

        <div class="row">
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon.png" alt=""></div>
              <h4><a href="">Aman</a></h4>
              <p>Emas dinilai sangat aman, jika kita memiliki uang dan hanya disimpan ditabungan, maka uang tersebut bisa perlahan menghilang. Hal tersebut dikarenakan adanya biaya administrasi, pajak, suku bunga, dan biaya lain-lain.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon-1.png" alt=""></div>
              <h4><a href="">Terlindungi</a></h4>
              <p>Inflasi dan deflasi merupakan masalah klasik yang sudah ada sejak dahulu kala. Dua kondisi ini bisa membuat nilai aset menurun. Maka dari itu, salah satu cara untuk mencegah hal tersebut yaitu dengan emas.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon-2.png" alt=""></div>
              <h4><a href="">Menguntungkan</a></h4>
              <p>Harga emas yang stabil bahkan cenderung meningkat, membuat banyak orang lebih memilih emas. Emas juga diketahui sangat cocok untuk disimpan dalam jangka menengah atau panjang.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon-3.png" alt=""></div>
              <h4><a href="">Mudah Dicairkan</a></h4>
              <p>Likuiditas yang tinggi menjadi salah satu alasan banyak orang berminat memiliki emas.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon-4.png" alt=""></div>
              <h4><a href="">Resiko Rendah</a></h4>
              <p>Emas diketahui tidak memiliki nilai penyusutan. Nilai emas untuk jangka pendek memang berfluktuasi, namun untuk jangka panjang nilainya cenderung meningkat.</p>
            </div>
          </div>
          <div class=" col-md-4 d-flex align-items-stretch text-center" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <!-- <div class="icon"><i class="bx bxl-dribbble"></i></div> -->
              <div class="icon"><img src="{{asset('assets/new2')}}/img/Featured icon-5.png" alt=""></div>
              <h4><a href="">Harga Stabil</a></h4>
              <p>Manfaat emas yaitu memiliki harga yang stabil. Emas memiliki risiko lebih rendah, keamanan ketat, dan keuntungan yang lebih besar dibandingkan produk lainnya.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <section id="product" class="product" style="background-color: #F5F5F5;">
      <div class="container" data-aos="fade-up">
        <div class="section-title mb-2 row justify-content-center">
          <h2 class="text-center">Produk Microgold</h2>
          <div class="col-md-4">
            <p>Microgold menyediakan emas bersertifikat dengan ukuran 0.01 gram, 0,02 gram, dan 0,05 gram.</p>
          </div>
        </div>
        <div class="owl-carousel owl-theme mb-5">
            @foreach ($prod as $item)
          <div class="item">
            <div class="card">
              <div class="row justify-content-center">
                <div class="image">
                    <img src="{{ getImage('assets/images/product/'. $item->image,  null, true)}}" class="img-fluid" alt="Image {{$item->name}}">
                </div>
                <h4 class="text-center mt-3 ">{{$item->name}}</h4>
                <h5>{{nbt($item->weight)}} gram</h5>
                <p class="text-center">Rp {{nb($item->price)}}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq bg-white">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
          <p>Semua yang perlu Anda ketahui tentang MicroGold.</p>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <a data-bs-toggle="collapse" class="collapse"
                data-bs-target="#faq-list-1">Apa Itu Microgold ? <i class="bx bx-plus-circle icon-show"></i></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                  Microgold adalah produsen dan penjual produk emas dengan ukuran micro : 0,01 gram, 0,02 gram, 0,05 gram. Fine gold 999 bersertifikat.
                  <br>
                  <br>

                  Pembeli produk microgold bisa menyimpan emas ukuran micro yang nanti bisa di tukar dengan emas antam 10 gram.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-2"
                class="collapsed">Apa itu Reseller Microgold ? <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Mitra yang memperoleh harga khusus jika membeli produk Microgold.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-3"
                class="collapsed">Bagaimana Cara Jika Saya Ingin Bergabung ? <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Silahkan Kunjungi website <a id="un" href="https://microgold.fund/">https://microgold.fund/</a>.
                  <br>
                  Klik menu “Registrasi” dan masukan data sesuai kolom yang tersedia dan buat password akun Anda. Microgold akan mengirimkan OTP melalui E-mail yang telah didaftarkan.
                  <br>
                  <br>
                  <strong>Selamat kamu telah berhasil bergabung bersama Microgold.</strong>
                  <br>
                  <br>
                  Silahkan Login menggunakan email dan password yang telah Anda daftarkan.
                  <br>
                  <br>
                  Jika terdapat kesalahan maupun kendala silahkan hubungi Customer Care Microgold di Whatsapps Chat 0812 8217 4631.
                </p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-delay="400">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-4"
                class="collapsed">Bagaimana Cara Membeli Produk Microgold ? <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                  •	Login ke akun pada Dashboard Microgold <br>
                •	Klik menu lalu pilih menu deposit now <br>
                •	Masukkan jumlah deposit yang diinginkan lalu submit dan mengikuti arahan deposit <br>
                •	Setelah saldo masuk, klik menu produk pada Dashboard Microgold <br>
                •	Pilih  product yang di inginkan dan jumlahnya <br>
                •	masukkan kode otp yang sudah dikirim melalui email, klik buy  <br>
                •	jika sudah semua, klik Gold accumulation <br>
                •	setelah itu klik add to delivery  <br>
                •	klik gambar truck yang disebelah profil, periksa kembali apakah sudah pas jumlahnya <br>
                •	klik submit dan muncul konfirmasi alamat dan ongkir yang tertera<br>
                •	lalu klik comfirm delivery <br>

                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="500">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-5"
                class="collapsed">Mengapa Saya Harus Melakukan Verifikasi Akun ? <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Seluruh pemilik akun Microgold harus melewati proses E-KYC guna perlindungan konsumen dan sebagai upaya komitmen Microgold dalam penerapan program APUPPT, sehingga Microgold memberlakukan kebijakan hanya Pengguna yang telah ter-verifikasi yang dapat melakukan deposit untuk pembelian paket Microgold.
                  <br><br>

                Jika terdapat kesalahan maupun kendala silahkan hubungi Customer Care Microgold di Whatsapps Chat 0812 8217 4631

                </p>
              </div>
            </li>
            {{-- <li data-aos="fade-up" data-aos-delay="600">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-6"
                class="collapsed">Berapa Pembelanjaan Minimal untuk menjadi Reseller Microgold ? <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-6" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Setelah melakukan pembelanjaan senilai Rp2.950.000,- , maka anda akan aktif menjadi reseller dan mendapat segala keuntungannya.
                  <br><br>
                  Jika terdapat kesalahan maupun kendala silahkan hubungi Customer Care Microgold di Whatsapps Chat 0812 8217 4631

                </p>
              </div>
            </li> --}}
            <li data-aos="fade-up" data-aos-delay="700">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-7"
                class="collapsed">Cara Melakukan Pemesanan Produk Custom Microgold <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-7" class="collapse" data-bs-parent=".faq-list">
                <p>
                  •	Masuk ke menu produk pada Dashboard Microgold <br>
                  •	Klik tombol order custom design di pojok anan atas <br>
                  •	Lengkapi form order custom design, lalu klik tombol purchase <br>
                  •	Admin akan approve order custom design Anda <br>
                  <br><br>
                  <strong>* Minimum order 100 pcs per design per gramasi</strong> 

                  <br>
                  Jika terdapat kesalahan maupun kendala silahkan hubungi Customer Care Microgold di Whatsapps Chat 0812 8217 4631.
                </p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-delay="800">
              <a data-bs-toggle="collapse" data-bs-target="#faq-list-8"
                class="collapsed">Cara Menukar Emas Sertifikat Dengan Emas Antam 5 Gram  <i
                  class="bx bx-plus-circle icon-show"></i><i class="bx bx-minus-circle icon-close"></i></a>
              <div id="faq-list-8" class="collapse" data-bs-parent=".faq-list">
                <p>
                  •	Konfirmasi melalui Customer Care Microgold  Microgold di Whatsapps Chat 0812 8217 4631 <br><br>
                  •	Kirimkan/bawa produk Microgold minimal 5 gram (kelipatan nya) yang ingin ditukarkan ke alamat kantor Microgold : <br>
                  o	STC Senayan, lantai 3 No 189, Jakarta Pusat
  
                  <br>
                  <br>
                  •	Admin akan melakukan verifikasi atas produk yang dikirimkan dengan hasil konfirmasi : <br>
                  o	Reject, produk akan dikembalikan ke alamat Customer (dikenakan biaya ongkos kirim). <br>
                  o	Approve, 
                  <br>
                  ~	Emas Antam akan dikirim ke alamat Customer (dikenakan biaya ongkos kirim) <br>
                  ~	Dapat diambil langsung saat penyerahan produk di kantor Microgold. <br>

                  <br>
                  Jika terdapat kesalahan maupun kendala silahkan hubungi Customer Care Microgold di Whatsapps Chat 0812 8217 4631.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row full-width-row">

          <div class="col-12 col-lg-3 col-md-3 footer-contact">
            <!-- <h3>Arsha</h3> -->
            <img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="" class="img-fluid logo-footer" >
            <p class="pb-3">STC SENAYAN LANTAI 3 NO. 167B. JALAN ASIA AFRIKA PINTU IX SENAYAN</p>
              <p>
                Desa/Kelurahan Gelora, Kec. Tanah Abang <br>
                Kota Adm. Jakarta Pusat 10270, Indonesia<br><br>
                <strong>Telepon:</strong> +62 812 8217 4631<br>
                <strong>Email:</strong> ptsakabhumiaurum@gmail.com <br>
              </p>
          </div>
          <div class="col-12 col-lg-3 col-md-6 footer-links">
            <h4>Layanan Pengaduan Konsumen</h4>
            <p>(Ditjen PKTN) Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga <br><br>
              Kementerian Perdagangan Republik Indonesia <br><br>
              Telp : +62 853 1111 1010</p>
          </div>

          <div class="col-12 col-lg-2 col-md-6 footer-links">
            <h4>Tautan Berguna</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#product">Produk</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about">Tentang</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#faq">FAQ</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://wa.me/+6281282174631" target="_blank">Kontak Kami</a></li>
            </ul>
          </div>

          <div class="col-12 col-lg-4 col-md-6 footer-links">
            <h4>Disclaimer:</h4>
            <p>PT Saka Bhumi Aurum adalah perusahaan produsen dan penjual produk emas fisik ukuran mikro (0.01 gr, 0.02 gr, 0.05 gr) dan PT Saka Bhumi Aurum bukan merupakan bagian atau mitra dari perusahaan Investasi Emas Online seperti (Lakuemas, Dinaran, Tamasia, Indogold) dan perusahaan sejenis lainnya.
            <br>
            <br>
            PT Saka Bhumi Aurum tidak memiliki media informasi ataupun media sosial apapun, perusahaan hanya memiliki website resmi dengan alamat https://microgold.fund
            <br>
            <br>
            Perusahaan tidak bertanggung jawab atas informasi diluar dari website resmi kami tersebut.
            </p>
          </div>

        </div>
      </div>
    </div>
    <!-- <hr> -->
    <div class="container footer-bottom clearfix ">
      <!-- <hr> -->
      <div class="copyright fweb">
        Copyright &copy; 2023 MicroGold. All Rights Reserved.
      </div>
      <div class="credits justify-content-end ">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        <a href="#" class="instagram"><i class="bx bxl-tiktok"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook-circle"></i></a>
          <a href="#" class="twitter"><i class="bx bxl-youtube"></i></a>
      </div>
      <div class="copyright fmob">
        Copyright &copy; 2023 MicroGold. All Rights Reserved.
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/new2')}}/vendor/aos/aos.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="{{asset('assets/new2')}}/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/new2')}}/js/main.js"></script>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <!-- Mengimpor library Owl Carousel JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script>
    $(document).ready(function () {
      var owl = $(".owl-carousel");

      // owl.owlCarousel({
      //   margin: 10,
      //   autoWidth: false,
      //   nav: false,
      //   items: 4,
      //   navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
      //     '<i class="fa fa-angle-right" aria-hidden="true"></i>'
      //   ]
      // })

      owl.owlCarousel({
        autoPlay: 3000,
        margin: 30,
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        center: true,
        nav:false,
        navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        loop:true,
        responsive: {
            0:{
            items:1,
            nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:4,
            nav:false,
        }
     }
    
      });
    });
  </script>
</body>

</html>