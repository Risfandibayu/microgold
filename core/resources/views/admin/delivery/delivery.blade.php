@extends('admin.layouts.app')

@section('panel')
<div class="row">
    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class="table-responsive--sm table-responsive">
                    <table class="table table--light">
                        <thead>
                            <tr>
                                <th scope="col">@lang('SL')</th>
                                <th scope="col">@lang('Time')</th>
                                <th scope="col">@lang('Delivery ID')</th>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('User Phone')</th>
                                {{-- <th scope="col">@lang('Product')</th> --}}
                                {{-- <th scope="col">@lang('Qty')</th> --}}
                                {{-- <th scope="col">@lang('Weight')</th> --}}
                                {{-- <th scope="col">@lang('Amount')</th> --}}
                                <th scope="col">@lang('Shipping Cost')</th>
                                {{-- <th scope="col">@lang('Charge')</th> --}}
                                {{-- <th scope="col">@lang('Post QTY')</th> --}}
                                <th scope="col">@lang('Address')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Product')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($items as $ex)
                            <tr>
                                <td data-label="@lang('SL')">{{ $items->firstItem()+$loop->index }}</td>
                                <td data-label="@lang('Date')">{{ showDateTime($ex->created_at) }}</td>
                                <td data-label="@lang('Delivery ID')">{{ $ex->trx }}</td>
                                <td data-label="@lang('User')"><a
                                        href="{{ route('admin.users.detail', $ex->user_id) }}">{{ $ex->user->username
                                        }}</a></td>
                                <td data-label="@lang('User Phone')">{{ $ex->user->mobile }}</td>
                                {{-- <td data-label="@lang('Product')">
                                    @if ($ex->is_custom == 1)
                                    {{ $ex->cname }}
                                    @else
                                    {{ $ex->pname }}
                                    @endif
                                </td>
                                <td data-label="@lang('Qty')">{{ nb($ex->qty) }}</td>
                                <td data-label="@lang('Weight')">{{ nbt($ex->pweight * $ex->qty) }} gr</td> --}}
                                <td data-label="@lang('Shipping Cost')">{{ nb($ex->ongkir) }}</td>
                                <td data-label="@lang('Address')">{{ Str::limit($ex->alamat,40) }}<button
                                        class="btn--info btn-rounded  badge detailAlm" data-alamat="{{$ex->alamat}}"
                                        data-pos="{{$ex->kode_pos}}" data-nhp="{{$ex->no_telp_penerima}}"
                                        data-np="{{$ex->nama_penerima}}"><i class="fa fa-info"></i></button></td>
                                <td data-label="@lang('Status')">
                                    @if($ex->status == 1)
                                    <span class="badge badge--success">@lang('Complete. On Delivery')</span>
                                    @elseif($ex->status == 2)
                                    <span class="badge badge--warning">@lang('Pending')</span>
                                    @elseif($ex->status == 3)
                                    <span class="badge badge--danger">@lang('Cancel')</span>
                                    @endif
                                    @if($ex->no_resi != null)
                                    <button class="btn--info btn-rounded  badge detailBtn"
                                        data-no_resi="{{$ex->no_resi}}"><i class="fa fa-info"></i></button>
                                    @endif
                                </td>
                                <td data-label="@lang('Product')"><button class="btn btn--info btn-sm det"
                                        id="det{{$ex->id}}" data-toggle="modal" data-target="#detailprod{{$ex->id}}">
                                        <i class="fas fa-info"></i>
                                        Detail</button></td>
                                <td>
                                    @if ($ex->status == 2)
                                    <button class="btn btn--primary btn-sm ml-1 devBtn" data-id="{{ $ex->id }}"
                                        data-toggle="tooltip" data-original-title="@lang('Deliver')"><i
                                            class="fas fa-paper-plane"></i>
                                        @lang('Deliver')
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ paginateLinks($items) }}
            </div>
        </div><!-- card end -->
    </div>
</div>


<div class="modal fade" id="exchange_verif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verification Exchange Request ID : <span
                        id="ex_id"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--danger reject">Reject</button>
                <button type="button" class="btn btn--primary accept">Accept</button>
            </div>
        </div>
    </div>
</div>

<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Nomor Resi : <span class="withdraw-detail"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>


<div id="deliverModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Approve Deposit Confirmation')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.deliver.deliver')}}" method="POST">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold" for="">Resi Number</label>
                            <input class="form-control " type="text" name="no_resi" id="no_resi" placeholder="No Resi"
                                required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                    <button type="submit" class="btn btn--success">@lang('Submit')</button>
                </div>
            </form>
        </div>
    </div>
</div>

@foreach ($items as $item)
<div id="detailprod{{$item->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- @foreach ($ex->detail as $item)

                @endforeach --}}
                <table class="table table--light style--two">
                    <thead>
                        <tr>
                            {{-- <th scope="col">@lang('SL')</th> --}}
                            <th scope="col">@lang('Product')</th>
                            <th scope="col" width="20%">@lang('Qty')</th>
                            <th scope="col" width="20%">@lang('Weight')</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($item->detail as $ex)
                        <tr>
                            {{-- <td data-label="@lang('SL')">{{ $devcart->firstItem()+$loop->index }}</td> --}}
                            <td data-label="@lang('Product')">
                                {{-- {{ $ex->gold->prod->name }} --}}
                                {{-- <div class="row d-flex">
                                    <div class="book">
                                        <img src="https://i.imgur.com/2DsA49b.jpg" class="book-img">
                                    </div>
                                    <div class="my-auto flex-column d-flex pad-left">
                                        <h6 class="mob-text">Thinking, Fast and Slow</h6>
                                    </div>
                                </div> --}}
                                @if ($ex->gold->is_custom == 0)
                                <div class="row d-flex">
                                    <div class="book">
                                        <img src="{{ getImage('assets/images/product/'. $ex->gold->prod->image,  null, true)}}"
                                            class="book-img">
                                    </div>
                                    <div class="my-auto flex-column d-flex pad-left">
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                            $ex->gold->prod->name }} </h6>
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Type : {{
                                            $ex->gold->prod->weight }} gr</h6>
                                    </div>
                                </div>
                                {{-- {{ $ex->gold->prod->name }} --}}
                                @else
                                <div class="row d-flex">
                                    <div class="book">
                                        <img src="{{ getImage('assets/images/cproduct/f/'. $ex->gold->cord->front,  null, true)}}"
                                            class="book-img">
                                    </div>
                                    <div class="my-auto flex-column d-flex pad-left">
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                            $ex->gold->cord->name}}</h6>
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Type : {{
                                            $ex->gold->prod->weight }} gr</h6>
                                    </div>
                                </div>
                                {{-- {{ $ex->gold->cord->name}} --}}

                                @endif

                            </td>
                            <td data-label="@lang('Qty')">
                                {{-- {{ $ex->qty }} --}}
                                <div class="input-group">

                                    <input type="text" readonly class="form-control input-number"
                                        value="{{ $ex->qty }} pcs" min="1" max="{{$ex->gold->qty}}">

                                </div>
                            </td>
                            <td data-label="@lang('Weight')">
                                <div class="input-group">

                                    <input type="text" readonly class="form-control input-number"
                                        value="{{ $ex->qty * $ex->gold->prod->weight }} gr" min="1"
                                        max="{{$ex->gold->qty}}">

                                </div>
                            </td>


                        </tr>
                        @empty
                        <tr>
                            <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endforeach

<div id="detailAlm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Alamat : <span class="withdraw-detail"></span>. <br> Kode Pos : <span
                        class="withdraw-detail1"></span>
                    <br> Nama Penerima : <span class="withdraw-detail3"></span>
                    <br> No Telp Penerima : <span class="withdraw-detail2"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    "use strict";
        (function ($) {
            $('.exchange_verif').on('click', function () {
                // console.log($(this).data('id'));
                var id = $(this).data('id');
                var modal = $('#exchange_verif');
                modal.find('#ex_id').html($(this).data('exid'));
                modal.find('.reject').on('click',function(){
                    document.getElementById("rform"+id).submit();
                    // console.log(id);
                });
                modal.find('.accept').on('click',function(){
                    document.getElementById("aform"+id).submit();
                    // console.log(id);
                });
        //         modal.find('#prices').val($(this).data('price'));
        //         modal.find('#price').html($(this).data('price'));
        //         modal.find('#weight').val($(this).data('weight'));
        //         modal.find('#tweight').html($(this).data('tweight'));
        //         modal.find('#product_id').val($(this).data('id'));
        //         modal.find('#product_name').val($(this).data('name'));

        //         modal.find('#qty').on('keyup change',function() { 
        // // alert('okl');
        //             modal.find('#total').val(modal.find('#qty').val() * modal.find('#weight').val().replace(/(\.\d{2})\d+/g, '$1'));
        //             modal.find('#total_rp').val(modal.find('#qty').val() * modal.find('#prices').val());
        //             modal.find('#totals').val(modal.find('#qty').val() * modal.find('#weight').val().replace(/(\.\d{2})\d+/g, '$1'));
        //         });
        //         // modal.find('.weight').val($(this).data('weight'));
        //         // var input = modal.find('.image');
        //         // // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

        //         // if($(this).data('status')){
        //         //     modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
        //         //     modal.find('input[name="status"]').prop('checked',true);

        //         // }else{
        //         //     modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
        //         //     modal.find('input[name="status"]').prop('checked',false);
        //         // }

        //         // modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            // $('.modal').on('hidden.bs.modal', function(){
            //     $(this).find('form')[0].reset();
            // });

            // $('.add-product').on('click', function () {
            //     var modal = $('#add-product');
            //     modal.modal('show');
            // });

            $('.devBtn').on('click', function () {
                var modal = $('#deliverModal');
                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            
        })(jQuery);

</script>
<script>
    $('.detailBtn').on('click', function () {
            var modal = $('#detailModal');
            var feedback = $(this).data('no_resi');
            modal.find('.withdraw-detail').html(feedback);
            modal.modal('show');
    });
</script>
<script>
    $('.detailAlm').on('click', function () {
            var modal = $('#detailAlm');
            var feedback = $(this).data('alamat');
            var pos = $(this).data('pos');
            var nhp = $(this).data('nhp');
            var np = $(this).data('np');
            modal.find('.withdraw-detail').html(feedback);
            modal.find('.withdraw-detail1').html(pos);
            modal.find('.withdraw-detail2').html(nhp);
            modal.find('.withdraw-detail3').html(np);
            modal.modal('show');
    });
</script>
@endpush

@push('breadcrumb-plugins')
<div class="row">

    <div class="col-md-8 col-12">

        <form action="" method="GET" class=" bg--white ml-2">
            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Username/BRO/Delivery ID')"
                    value="{{ $search ?? '' }}">
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
        <form action="" method="GET" class=" bg--white ">
            <div class="input-group has_append">
                <select name="status" id="status" class="from-select">
                    <option disabled {{ $sts=='' ? 'selected' : '' }}>Filter by Status</option>
                    <option value="pending" {{ $sts=='pending' ? 'selected' : '' }}>Pending
                    </option>
                    <option value="success" {{ $sts=='success' ? 'selected' : '' }}>Success
                    </option>
                </select>

                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4 col-12">
        <form action="{{ route('admin.export.delivery') }}" method="GET" class="">
            <input hidden type="text" name="search" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $search ?? '' }}">
            <input hidden type="text" name="status" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $sts ?? '' }}">
            <button class="btn btn--primary" type="submit">Export</button>
        </form>
    </div>
</div>
@endpush