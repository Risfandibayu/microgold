@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Voucher Name')</th>
                                <th scope="col">@lang('Voucher Code')</th>
                                <th scope="col">@lang('Use For')</th>
                                <th scope="col">@lang('Percent')</th>
                                <th scope="col">@lang('Nominal')</th>
                                <th scope="col">@lang('Active Until')</th>
                                {{-- <th scope="col">@lang('Status')</th> --}}
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($vouchers as $key => $v)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>

                                <td data-label="@lang('Voucher Name')">{{ $v->name }}</td>
                                <td data-label="@lang('Voucher Code')">{{ $v->kode_voucher }}</td>
                                <td data-label="@lang('Use For')">{{ $v->for }}</td>
                                <td data-label="@lang('Percent')">{{ $v->persen }}</td>
                                <td data-label="@lang('Nominal')">{{ $v->nominal }}</td>
                                <td data-label="@lang('Active Until')">{{ $v->aktif_sampai }}</td>
                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-original-title="Delete">
                                        <i class="la la-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $vouchers->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>


{{-- edit modal--}}
<div id="edit-product" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Product')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.products.update')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Product Image') <small>(recommended image ratio
                                    9:16)</small></label>
                            <input class="form-control form-control-lg image" type="file" accept="image/*"
                                onchange="loadFile(event)" name="images">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name" placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Price') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control price" placeholder="10000" name="price" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type')</label>
                            <input type="number" class="form-control weight" name="weight" step="0.001"
                                placeholder="0.001" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Stock')</label>
                            <input type="number" class="form-control stok" name="stok" step="0" placeholder="0">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Pricing For')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Reseller')" data-off="@lang('Public')"
                                name="reseller" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="add-voucher" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Voucher')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('admin.voucher.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control" name="name" placeholder="Voucher Name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Voucher Code')</label> <small>(if empty voucher code will generate automatically)</small>
                            <input type="text" class="form-control" name="code" placeholder="Voucher Code">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Percent Voucher')</label>
                            {{-- <input type="number" class="form-control" name="persen" min="0" max="99" step="0"
                                placeholder="0"> --}}
                            <div class="input-group">
                                <input type="number" min="0" max="99" name="persen" class="form-control" placeholder="0"
                                    aria-label="0" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Nominal Voucher')</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Rp. </span>
                                </div>
                                <input type="number" class="form-control" name="nominal" placeholder="000.000.000"
                                    aria-label="000.000.000" aria-describedby="basic-addon1" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Used For')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-primary"
                                data-toggle="toggle" data-on="@lang('BRO Plan')" data-off="@lang('Retail')" name="for"
                                checked>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Active Until')</label>
                            <input type="datetime-local" class="form-control" name="aktif_sampai" placeholder="0000000">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-voucher"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush

@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                console.log($(this).data('image'));
                var modal = $('#edit-product');
                modal.find('.name').val($(this).data('name'));
                modal.find('.price').val($(this).data('price'));
                modal.find('.stok').val($(this).data('stok'));
                modal.find('.weight').val($(this).data('weight'));
                var input = modal.find('.image');
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                if($(this).data('status')){
                    modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
                    modal.find('input[name="status"]').prop('checked',true);

                }else{
                    modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
                    modal.find('input[name="status"]').prop('checked',false);
                }

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-voucher').on('click', function () {
                var modal = $('#add-voucher');
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush