@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body p-0 text-center">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($category as $key => $cat)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($cat->name) }}</td>

                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edits" data-toggle="tooltip"
                                        data-id="{{ $cat->id }}"
                                        data-name="{{ $cat->name }}"
                                        data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                    <button type="button" class="icon-btn" data-toggle="tooltip"
                                            onclick="window.location='{{route('admin.category.delete',[$cat->id])}}'">
                                            <i class="la la-trash"></i>
                                        </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $category->links('admin.partials.paginate') }}
            </div>
        </div>

    </div>
</div>


<div id="add-category" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Product Category')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('admin.category.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Name')</label>
                            <input type="text" class="form-control name" name="name" placeholder="category name" required>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="edit-category" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Product Category')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('admin.category.edit')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Name')</label>
                            <input type="text" class="form-control name" name="name" placeholder="category name" required>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-category"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush

@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                console.log($(this).data('image'));
                var modal = $('#edit-product');
                modal.find('.name').val($(this).data('name'));
                modal.find('.price').val($(this).data('price'));
                modal.find('.stok').val($(this).data('stok'));
                modal.find('.weight').val($(this).data('weight'));
                var input = modal.find('.image');
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                if($(this).data('reseller') == 0){
                    modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
                    modal.find('input[name="reseller"]').prop('checked',true);

                }else{
                    modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
                    modal.find('input[name="reseller"]').prop('checked',false);
                }

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-category').on('click', function () {
                var modal = $('#add-category');
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                var modal = $('#edit-category');
                modal.find('.name').val($(this).data('name'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush