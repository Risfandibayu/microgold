@extends('admin.layouts.app')

@section('panel')
    <div class="row">

        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--sm table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                            <tr>
                                <th scope="col">@lang('#')</th>
                                <th scope="col">@lang('Claimed At')</th>
                                <th scope="col">@lang('Ticket ID')</th>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('No BRO')</th>
                                <th scope="col">@lang('Reward')</th>
                                {{-- <th scope="col">@lang('MP Kiri')</th> --}}
                                {{-- <th scope="col">@lang('MP Kanan')</th> --}}
                                {{-- <th scope="col">@lang('Action')</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users  as $ex)
                                <tr>
                                    <td data-label="@lang('#')">{{ $users->firstItem()+$loop->index }}</td>
                                    <td data-label="@lang('Claimed At')">{{ showDateTime($ex->created_at) }}</td>
                                    <td data-label="@lang('Reward')">{{ $ex->trx }}</td>
                                    <td data-label="@lang('User')"><a href="{{ route('admin.users.detail', $ex->user->id) }}">{{ $ex->user->username }}</a></td>
                                    <td data-label="@lang('No BRO')">{{ $ex->user->no_bro }}</td>
                                    <td data-label="@lang('Reward')">{{ $ex->rewa->reward }}</td>
                                    {{-- <td data-label="@lang('MP Kiri')">{{ $ex->rewa->kiri }}</td> --}}
                                    {{-- <td data-label="@lang('MP Kanan')">{{ $ex->rewa->kanan }}</td> --}}
                                    {{-- <td data-label="@lang('Action')">
                                        <a class="btn btn--success" href="{{ route('user.ticket.print', $ex->id) }}" target="_blank" class="print"><i class="las la-print"></i></a>
                                    </td> --}}
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ $users->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
@push('breadcrumb-plugins') 
<div class="row">
    <div class="col-md-3 col-3">
            <a href="{{route('admin.user.reward.scan')}}" class="btn btn--primary">Scan Barcode <i class="fa fa-camera"></i></a>
    </div>
    
    <div class="col-md-9 col-9">
        <form action="" method="GET" class="form-inline float-sm-right bg--white ml-2">

            <div class="input-group has_append">
                <input type="text" name="search" class="form-control form--control-2" placeholder="@lang('Username/Ticket ID')" value="{{ $search ?? '' }}">
                @if (isset($reward))
                    <select name="reward_id" id="reward_id" class="from-select  form--control-2">
                        <option disabled {{ $reward == '' ? 'selected' : '' }}>Filter by reward</option>
                        @foreach ($listreward as $item)
                        <option value="{{$item->id}}"{{ $reward == $item->id ? 'selected' : '' }}>{{$item->reward}}
                        </option>
                        @endforeach
                    </select>
                @else
                    <select name="reward_id" id="reward_id" class="from-select">
                        <option disabled selected>Filter by reward</option>
                        @foreach ($listreward as $item)
                        <option value="{{$item->id}}">{{$item->reward}}
                        </option>
                        @endforeach
                    </select>
                @endif

                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
        
    </div>
</div>
@endpush
