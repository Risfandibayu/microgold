
<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ $general->sitename($page_title ?? '') }}</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="author" content="">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link rel="shortcut icon" type="image/png" href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/guest')}}/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/guest')}}/fonts/icomoon.css">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/guest')}}/css/vendor.css">
  <!--Bootstrap ================================================== -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="{{asset('assets/guest')}}/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Syne:wght@600;700;800&display=swap" rel="stylesheet">
  <link
    href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&family=Syne:wght@600;700;800&display=swap"
    rel="stylesheet">
</head>

<body class="single-page shop-single header-style2 bg-accent-light">
  <header id="header" class=" bg-light ">
    <div class="header-wrap container py-3">
      <div class="row align-items-center">
        <div class="primary-nav col-md-5 col-sm-2">
          
        </div>
        <div class="col-md-2 col-sm-4 brand-block ">
          <div class="main-logo text-lg-center">
            <a href="index.html">
              <img src="{{asset('assets/guest')}}/images/logo.png" alt="logo" class="brand-image">
            </a>
          </div>
        </div>
        <div class="right-block col-md-5 col-sm-4">
        </div>
      </div>
    </div>
  </header>
  <section class="hero-section style2 bg-accent padding-medium">
    <div class="hero-content d-flex justify-content-center text-center align-items-center">
      <div class="container">
        <div class="row pb-4">
          <div class="text-center">
            <h1 class="page-title">Product Detail</h1>
            <div class="breadcrumbs">
              <span class="item">
                <a href="#">Product ></a>
              </span>
              <span class="item">Detail</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="selling-product" class="single-product overflow-hidden padding-large">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="product-preview">
            <div class="swiper large-swiper overflow-hidden col-md-10 col-sm-10">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img src="{{ getImage('assets/images/product/'. $product->image,  null, true)}}" alt="single-product">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="product-info">
            <div class="element-header">
              <h3 itemprop="name" class="product-title no-margin">{{$product->name}}</h3>
              <div class="rating-container d-flex align-items-center">
                <div class="rating" data-rating="1" onclick=rate(1)>
                  <i class="icon icon-star"></i>
                </div>
                <div class="rating" data-rating="2" onclick=rate(1)>
                  <i class="icon icon-star"></i>
                </div>
                <div class="rating" data-rating="3" onclick=rate(1)>
                  <i class="icon icon-star"></i>
                </div>
                <div class="rating" data-rating="4" onclick=rate(1)>
                  <i class="icon icon-star"></i>
                </div>
                <div class="rating" data-rating="5" onclick=rate(1)>
                  <i class="icon icon-star-line"></i>
                </div>
              </div>
            </div>
            <div class="product-price">
              <strong class="text-primary">Rp. {{nb($product->price)}}</strong>
              <del>Rp. {{nb($product->price + 20000)}}</del>
            </div>
            <p>{{$product->deskripsi}}</p>
            <div class="cart-wrap border-top border-bottom padding-small">
              <div class="product-quantity">
                <div class="stock-button-wrap d-flex flex-wrap">
                  <a href="{{route('user.register')}}?ref={{$ref}}"  type="submit" name="add-to-cart" value="1269"
                    class="btn btn-black btn-medium text-uppercase no-margin">Buy</a>
                </div>
              </div>
            </div>
            <div class="meta-product padding-small">
              <div class="meta-item d-flex align-items-baseline">
                <h4 class="item-title no-margin">Type:</h4>
                <ul class="select-list list-unstyled d-flex">
                  <li data-value="S" class="select-item">
                    <a href="#">{{nbt($product->weight)}} Gram</a>
                  </li>
                </ul>
              </div>
              <div class="meta-item d-flex align-items-baseline">
                <h4 class="item-title no-margin">Category:</h4>
                <ul class="select-list list-unstyled d-flex">
                  <li data-value="S" class="select-item">
                    <a href="#">{{$product->cat->name ?? 'N/A'}}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="product-tabs">
    <div class="container">
      <div class="row">
        <div class="tabs-listing">
          <nav>
            <div class="nav nav-tabs d-flex flex-wrap justify-content-center" id="nav-tab" role="tablist">
              <button class="nav-link active text-capitalize" id="nav-home-tab" data-bs-toggle="tab"
                data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                aria-selected="true">Description</button>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active border-top border-bottom padding-small" id="nav-home" role="tabpanel"
              aria-labelledby="nav-home-tab">
              <p>Product Description</p>
              <p>{{$product->deskripsi}}</p>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  <footer id="footer" class="overflow-hidden">
    <div class="container mt-5">
      <div class="row">
        <div class="footer-top-area">
          <div class="row d-flex flex-wrap justify-content-between">
            <div class="col-lg-3 col-sm-6">
              <div class="menu-001 text-center">
                <img src="{{asset('assets/guest')}}/images/logo.png" class="img-fluid mb-3" alt="logo">
                <p class="pb-3 mt-2">STC SENAYAN LANTAI 3 NO. 167B. JALAN ASIA AFRIKA PINTU IX SENAYAN</p>
                <p>
                    Desa/Kelurahan Gelora, Kec. Tanah Abang <br>
                    Kota Adm. Jakarta Pusat 10270, Indonesia<br><br>
                    <strong>Telepon:</strong> +62 812 8217 4631<br>
                    <strong>Email:</strong> ptsakabhumiaurum@gmail.com <br>
                </p>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6">
              <div class="footer-menu menu-002">
                <h5 class="widget-title">Layanan Pengaduan Konsumen</h5>
                <p>(Ditjen PKTN) Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga <br><br>
                    Kementerian Perdagangan Republik Indonesia <br><br>
                    Telp : +62 853 1111 1010</p>
              </div>
            </div>
            <div class="col-lg-5 col-sm-6">
              <div class="footer-menu contact-item menu-004">
                <h5 class="widget-title">Disclaimer</h5>
                <p>PT Saka Bhumi Aurum adalah perusahaan produsen dan penjual produk emas fisik ukuran mikro (0.01 gr, 0.02 gr, 0.05 gr) dan PT Saka Bhumi Aurum bukan merupakan bagian atau mitra dari perusahaan Investasi Emas Online seperti (Lakuemas, Dinaran, Tamasia, Indogold) dan perusahaan sejenis lainnya.
                    <br>
                    <br>
                    PT Saka Bhumi Aurum tidak memiliki media informasi ataupun media sosial apapun, perusahaan hanya memiliki website resmi dengan alamat https://microgold.fund
                    <br>
                    <br>
                    Perusahaan tidak bertanggung jawab atas informasi diluar dari website resmi kami tersebut.
                    </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
  </footer>
  <div id="footer-bottom">
    <div class="container text-center">
        <div class="copyright" style="    border-top: white;">
            Copyright &copy; 2022 <strong><span>MicroGold</span></strong>. All Rights Reserved
          </div>
    </div>
  </div>
  <script src="{{asset('assets/guest')}}/js/jquery-1.11.0.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
  <script src="{{asset('assets/guest')}}/js/plugins.js"></script>
  <script src="{{asset('assets/guest')}}/js/script.js"></script>
  <!-- Bootstarp script ================================================== -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
    crossorigin="anonymous"></script>
</body>

</html>