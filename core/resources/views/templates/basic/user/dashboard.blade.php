@extends($activeTemplate . 'user.layouts.app')

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
<style>
    :root {
        --primary-color: rgb(11, 78, 179)
    }


    label {
        display: block;
        margin-bottom: 0.5rem
    }

    input {
        display: block;
        width: 100%;
        padding: 0.75rem;
        border: 1px solid #ccc;
        border-radius: 0.25rem;
        height: 50px
    }

    .width-50 {
        width: 50%
    }

    .ml-auto {
        margin-left: auto
    }

    .text-center {
        text-align: center
    }

    .progressbar {
        position: relative;
        display: flex;
        justify-content: space-between;
        counter-reset: step;
        margin: 2rem 2rem 4rem
    }

    .progressbar::before,
    .progress {
        content: "";
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        height: 4px;
        width: 100%;
        background-color: #dcdcdc;
        z-index: 1
    }

    .progress {
        background-color: rgb(0 128 0);
        width: 0%;
        transition: 0.3s
    }

    .progress-step {
        width: 2.1875rem;
        height: 2.1875rem;
        background-color: #dcdcdc;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1
    }

    .progress-step::before {
        counter-increment: step;
        content: counter(step)
            /* font-family: FontAwesome; */
            /* content: "\f023"; */

    }

    .progress-step::after {
        content: attr(data-title);
        position: absolute;
        top: calc(100% + 0.5rem);
        font-size: 0.85rem;
        color: #666;
        width: 80px;
    }

    .progress-step-active {
        background-color: var(--primary-color);
        color: #f3f3f3
    }

    .form {
        width: clamp(320px, 30%, 430px);
        margin: 0 auto;
        border: none;
        border-radius: 10px !important;
        overflow: hidden;
        padding: 1.5rem;
        background-color: #fff;
        padding: 20px 30px
    }

    .step-forms {
        display: none;
        transform-origin: top;
        animation: animate 1s
    }

    .step-forms-active {
        display: block
    }

    .group-inputs {
        margin: 1rem 0
    }

    @keyframes animate {
        from {
            transform: scale(1, 0);
            opacity: 0
        }

        to {
            transform: scale(1, 1);
            opacity: 1
        }
    }

    .btns-group {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        gap: 1.5rem
    }

    .btn {
        padding: 0.75rem;
        display: block;
        text-decoration: none;
        background-color: var(--primary-color);
        color: #f3f3f3;
        text-align: center;
        border-radius: 0.25rem;
        cursor: pointer;
        transition: 0.3s
    }

    .btn:hover {
        box-shadow: 0 0 0 2px #fff, 0 0 0 3px var(--primary-color)
    }

    .progress-step-check {
        position: relative;
        background-color: green !important;
        transition: all 0.8s
    }

    .progress-step-check::before {
        position: absolute;
        content: '\2713';
        width: 100%;
        height: 100%;
        top: 8px;
        left: 13px;
        font-size: 12px
    }

    .group-inputs {
        position: relative
    }

    .group-inputs label {
        font-size: 13px;
        position: absolute;
        height: 19px;
        padding: 4px 7px;
        top: -14px;
        left: 10px;
        color: #a2a2a2;
        background-color: white
    }

    .welcome {
        height: 450px;
        width: 350px;
        background-color: #fff;
        border-radius: 6px;
        display: flex;
        justify-content: center;
        align-items: center
    }

    .welcome .content {
        display: flex;
        align-items: center;
        flex-direction: column
    }

    .checkmark__circle {
        stroke-dasharray: 166;
        stroke-dashoffset: 166;
        stroke-width: 2;
        stroke-miterlimit: 10;
        stroke: #7ac142;
        fill: none;
        animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards
    }

    .checkmark {
        width: 56px;
        height: 56px;
        border-radius: 50%;
        display: block;
        stroke-width: 2;
        stroke: #fff;
        stroke-miterlimit: 10;
        margin: 10% auto;
        box-shadow: inset 0px 0px 0px #7ac142;
        animation: fill .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both
    }

    .checkmark__check {
        transform-origin: 50% 50%;
        stroke-dasharray: 48;
        stroke-dashoffset: 48;
        animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards
    }

    @keyframes stroke {
        100% {
            stroke-dashoffset: 0
        }
    }

    @keyframes scale {

        0%,
        100% {
            transform: none
        }

        50% {
            transform: scale3d(1.1, 1.1, 1)
        }
    }

    @keyframes fill {
        100% {
            box-shadow: inset 0px 0px 0px 30px #7ac142
        }
    }
    .custom-btn {
        /* width: 130px; */
        /* height: 40px; */
        color: #fff;
        border-radius: 5px;
        /* padding: 10px 25px; */
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: transparent;
        /* cursor: pointer; */
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgba(255, 255, 255, .5),
            7px 7px 20px 0px rgba(0, 0, 0, .1),
            4px 4px 5px 0px rgba(0, 0, 0, .1); */
        outline: none;
    }

    /* 11 */
    .btn-11 {
        display: inline-block;
        outline: none;
        font-family: inherit;
        font-size: 1em;
        box-sizing: border-box;
        border: none;
        border-radius: .3em;
        /* height: 2.75em; */
        /* line-height: 2.5em; */
        text-transform: uppercase;
        /* padding: 0 1em; */
        /* box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(110, 80, 20, .4),
            inset 0 -2px 5px 1px rgba(139, 66, 8, 1),
            inset 0 -1px 1px 3px rgba(250, 227, 133, 1); */
        background-image: linear-gradient(160deg, #a54e07, #dda431, #eedd6d, #e2aa2f, #a54e07);
        /* border: 1px solid #a55d07; */
        color: rgb(120, 50, 5);
        text-shadow: 0 2px 2px rgba(250, 227, 133, 1);
        /* cursor: pointer; */
        transition: all .2s ease-in-out;
        background-size: 100% 100%;
        background-position: center;
        overflow: hidden;
    }

    /* .btn-11:hover {
    text-decoration: none;
    color: #fff;
} */
    .btn-11:before {
        position: absolute;
        content: '';
        display: inline-block;
        top: -180px;
        left: 0;
        width: 30px;
        height: 100%;
        background-color: #fff;
        animation: shiny-btn1 3s ease-in-out infinite;
    }

    /* .btn-11:hover{
  opacity: .7;
} */
    /* .btn-11:active{
  box-shadow:  4px 4px 6px 0 rgba(255,255,255,.3),
              -4px -4px 6px 0 rgba(116, 125, 136, .2), 
    inset -4px -4px 6px 0 rgba(255,255,255,.2),
    inset 4px 4px 6px 0 rgba(0, 0, 0, .2);
} */


    @-webkit-keyframes shiny-btn1 {
        0% {
            -webkit-transform: scale(0) rotate(45deg);
            opacity: 0;
        }

        80% {
            -webkit-transform: scale(0) rotate(45deg);
            opacity: 0.5;
        }

        81% {
            -webkit-transform: scale(4) rotate(45deg);
            opacity: 1;
        }

        100% {
            -webkit-transform: scale(50) rotate(45deg);
            opacity: 0;
        }
    }

    .desktop-img {
			display: block;
			width: 100%;
			height: auto;
		}

		/* gambar untuk tampilan seluler */
		.mobile-img {
			display: none;
		}

		/* media query untuk tampilan seluler */
		@media only screen and (max-width: 600px) {
			.desktop-img {
				display: none;
			}

			.mobile-img {
				display: block;
				width: 100%;
				height: auto;
			}
		}
</style>
@endpush
@push('script')

@endpush
@section('panel')
<div class="row">
    @if(Auth::user()->is_kyc == 0)
    <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step" data-title="Verification"></div>
                    <div class="progress-step" data-title="Status"></div>
                </div>
                <p>It is recommended to verify to be able to unlock all features.</p>
                <a href="{{route('user.verification')}}" class="btn btn-sm btn-danger">Verify Now</a>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->is_kyc == 1)
    <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step" data-title="Status"></div>
                </div>
                <p>Your data is in the verification process.</p>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->is_kyc == 2)
    <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step progress-step-active bg-success" data-title="Verified"></div>
                </div>
                <p>Your data has been successfully verified.</p>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->is_kyc == 3)
    <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step progress-step-active bg-danger" data-title="Rejected"></div>
                </div>
                <p>Your data failed to verify, please resend your data.</p>
                <p> <small>See KYC Reject Reason </small> <button class="btn-info btn-rounded badge detail" data-note="{{Auth::user()->kyc_note}}"><i
                    class="fa fa-info"></i></button> </p> 
                <a href="{{route('user.verification')}}" class="btn btn-sm btn-danger">Resend Data</a>
            </div>
        </div>
    </div>
    @endif

    @if (Auth::user()->plan_id != 0)
    <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                BRO Number
            </div>
            <div class="card-body fs-1 text-center bg--gradi-9" style="border-radius: 0 0 6px 6px;">
                <h2 style="font-weight: 700;color: black;line-height: 5; font-size: 33px;">{{Auth::user()->no_bro}}</h2>
            </div>
        </div>
    </div>
    @endif
    {{-- <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Title
            </div>
            <div class="card-body text-center custom-btn btn-11" style="border-radius: 0 0 6px 6px;">
                <img class="img-fluid w-50" src="{{ getImage('assets/images/leader.png', null, true) }}" alt="">
            </div>
        </div>
    </div> --}}
</div>
<div class="row mb-none-30">


    {{-- @if($general->free_user_notice != null)
    <div class="col-lg-12 col-sm-6 mb-30">
        <div class="card border--light">
            @if($general->notice == null)
            <div class="card-header">@lang('Notice')</div> @endif
            <div class="card-body">
                <p class="card-text"> @php echo $general->free_user_notice; @endphp </p>
            </div>
        </div>
    </div>
    @endif --}}
    @if (auth()->user()->plan_id != 0)

    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 text-center">
        <div class="dashboard-w1  h-100 w-100 bg--gradi-18 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-tree"></i>
            </div>
            <div class="details">
                <div class="numbers" >
                    <span class="amount">{{nb(intval((auth()->user()->userExtra->left + auth()->user()->userExtra->right)))}}</span>
                    <span class="currency-sign">BRO</span>
                </div>
                <div class="desciption">
                    <span class="text--small">Total BRO Joined</span>
                </div>
            </div>
            <br>
            <a href="{{ route('user.my.tree') }}" class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3" >@lang('View All')</a>
        </div>
    </div> --}}

    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 text-center">
        <div class="dashboard-w1  h-100 w-100 bg--gradi-18 b-radius--10 box-shadow">
            <div class="details">
                <div class="numbers">
                    <span class="amount" style="font-family: serif;
                    font-weight:bold;background-color: #414141ec;
                    color: transparent;
                    text-shadow: 0px 2px 3px rgba(255, 255, 255, 0.007);
                    -webkit-background-clip: text;
                       -moz-background-clip: text;
                            background-clip: text;  ">{{auth()->user()->bro_qty}} BRO</span>
                </div>
            </div>
            <div class="icon">
                <i class="las la-tree"></i>
            </div>
            <div class="details">
                <div class="numbers" >
                    <span class="amount">{{nb(auth()->user()->bro_qty + 1)}}</span>
                    <span class="currency-sign">BRO</span>
                </div>
                <div class="desciption">
                    <span class="text--small">Business Right Owner</span>
                </div>
            </div>
            <br>
            <a href="{{ route('user.my.tree') }}" class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div> --}}
    @endif

    @if ($emas)
        
    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--gradi-1 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-coins"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nbt($emas->total_wg)}}</span>
                    <span class="currency-sign">Gram</span>
                </div>
                <div class="desciption">
                    <span class="text--small">Equal To {{nb($emas->total_rp)}} IDR</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.gold.invest')}}" class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3" >@lang('View All')</a>
        </div>
    </div>
    @endif

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--success b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-wallet"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nb(getAmount(auth()->user()->balance))}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Current Balance')</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.report.transactions')}}" class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--gradi-4  b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-wallet"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nb(getAmount(auth()->user()->balance_on_hold))}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Balance On hold')</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.report.transactions')}}"
                class="btn btn-sm btn-block text--small bg--white text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--primary b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-cloud-upload-alt "></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nb(getAmount($totalDeposit))}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Deposit')</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.report.deposit')}}"
                class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--10 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-cloud-download-alt"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nb(getAmount($totalWithdraw))}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Withdraw')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.withdraw')}}"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>
    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--teal b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-check"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{$completeWithdraw}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Complete Withdraw')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.withdraw')}}?type=complete"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--warning b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-spinner"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{$pendingWithdraw}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Pending Withdraw')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.withdraw')}}?type=complete"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--danger b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-ban"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{$rejectWithdraw}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Reject Withdraw')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.withdraw')}}?type=reject"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--cyan b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-money-bill-wave"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{nb(getAmount(auth()->user()->total_invest))}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Transaction')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.invest')}}"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3" >@lang('View All')</a>
        </div>
    </div>

    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--12 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-money-bill"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{getAmount(auth()->user()->total_ref_com)}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Referral Commission')</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.report.refCom')}}"
                class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div> --}}

    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--info b-radius--10 box-shadow">
            <div class="icon">
                <i class="fa fa-tree"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    
                    <span class="amount">{{nb(getAmount(auth()->user()->total_binary_com))}}</span>

                    
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Commission')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.report.binaryCom')}}"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3" >@lang('View All')</a>
        </div>
    </div> --}}

    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--3 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-users"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{$total_ref}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Referral')</span>
                </div>
            </div>
            <br>
            <a href="{{route('user.my.ref')}}"
                class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div> --}}


    {{-- left right --}}
    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--15 b-radius--10 box-shadow">
            <div class="icon">
                <i class="fa fa-arrow-circle-left"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{auth()->user()->userExtra->left}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Group A')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.my.tree')}}"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--10 b-radius--10 box-shadow">
            <div class="icon">
                <i class="fa fa-arrow-circle-right"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{auth()->user()->userExtra->right}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Group B')</span>
                </div>
            </div>
            <br>
                <a href="{{route('user.my.tree')}}"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
        </div>
    </div> --}}


    {{-- <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--17 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-cart-arrow-down"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{auth()->user()->userExtra->bv_left +
                        auth()->user()->userExtra->bv_right}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total BV')</span>
                </div>
                <a href="{{route('user.bv.log')}}?type=paidBV"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--19 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-arrow-alt-circle-left"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{getAmount(auth()->user()->userExtra->bv_left)}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Left BV')</span>
                </div>
                <a href="{{route('user.bv.log')}}?type=leftBV"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--11 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-arrow-alt-circle-right"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{getAmount(auth()->user()->userExtra->bv_right)}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Right BV')</span>
                </div>
                <a href="{{route('user.bv.log')}}?type=rightBV"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
        <div class="dashboard-w1 bg--13 b-radius--10 box-shadow">
            <div class="icon">
                <i class="las la-hand-holding-usd"></i>
            </div>
            <div class="details">
                <div class="numbers">
                    <span class="amount">{{getAmount($totalBvCut)}}</span>
                    <span class="currency-sign">{{$general->cur_text}}</span>
                </div>
                <div class="desciption">
                    <span class="text--small">@lang('Total Bv Cut')</span>
                </div>
                <a href="{{route('user.bv.log')}}?type=cutBV"
                    class="btn btn-sm text--small bg--white btn-block text--black box--shadow3 mt-3">@lang('View All')</a>
            </div>
        </div>
    </div> --}}
    @if ($isReward)
    <div class="col-xl-12 col-lg-12 col-sm-12 mb-30">
            <div class="card bg--gradi-51">
                <div class="card-header">
                    <h2 class="card-title text-center text-dark">Reward List</h2>
                    <h6 class="card-title text-center text-dark mt-n3">List of Rewards that can be claimed from the left and right of your sales group.</h6>
                    <hr class="text-light">
                    <div class="row d-flex justify-content-center text-center">
                        {{-- <div class="col-md-4 mt-3 text-center">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('assets/turki.jpg') }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">150 Kiri : 150 Kanan</h5>
                                <p class="card-text">Dapatkan Reward <b>Trip Ke Turki</b> Dengan 150:150 Downline</p>
                                <a href="#"
                                    class="btn btn-primary  @if (auth()->user()->userExtra->left <= 150 && auth()->user()->userExtra->right <= 150) disabled @endif">Ambil
                                    Reward</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-3">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('assets/cars.jpeg') }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">999 Kiri : 999 Kanan</h5>
                                <p class="card-text">Dapatkan Reward <b>Mobil Wuling Almaz</b> Dengan 999:999
                                    Downline
                                </p>
                                <a href="#"
                                    class="btn btn-primary @if (auth()->user()->userExtra->left <= 999 && auth()->user()->userExtra->right <= 999) disabled @endif">Ambil
                                    Reward</a>
                            </div>
                        </div>
                    </div> --}}
                        @foreach ($reward as $item)
                            {{-- {{$item->id}} --}}
                            @if (auth()->user()->userExtra->left < $item->kiri && auth()->user()->userExtra->right < $item->kanan)
                            @else
                                {{-- @if (cekReward($item->id) == 1)
                            2
                        @else
                            1
                        @endif --}}
                                {{-- {{$item->id}} --}}
                                <div class="col-md-4 mt-3 text-center">
                                    <div class="card">
                                        <img class="card-img-top"
                                            src="{{ getImage('assets/images/reward/' . $item->images, null, true) }}"
                                            alt="Bonus reward {{ $item->reward }}">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $item->kiri }} Left : {{ $item->kanan }}
                                                Right</h5>
                                            <p class="card-text">Get <b>{{ $item->reward }}</b> Reward With
                                                {{ $item->kiri }}:{{ $item->kanan }} Downlines</p>
                                                <p class="card-text">Your Available Downline For Claim {{auth()->user()->userExtra->left - auth()->user()->reward_claim}}:{{auth()->user()->userExtra->right - auth()->user()->reward_claim}}</p>
                                                {{-- @dump(intval(cekAvClaim())); --}}
                                            {{-- @if (intval(cekAvClaim()) )
                                                
                                            @endif --}}
                                            {{-- @if (cekReward($item->id) == 1)
                                                    @if (intval($weak/$item->kiri) - cekRewardClaimed($item->id) > 0)
                                                        @if (intval(cekAvClaim()/$item->kiri) > 0)
                                                            <form method="post"
                                                                action="{{ route('user.claim.rewardup', $item->id) }}">
                                                                @csrf
                                                                
                                                                <input name="qty" type="hidden" value="{{intval($weak/$item->kiri) - cekRewardClaimed($item->id)}}">
                                                                <b class="text-dark">{{intval($weak/$item->kiri) - cekRewardClaimed($item->id)}} Reward Available To Claim</b>
                                                                <button type="submit"
                                                                    class="btn btn-primary btn-block  @if (auth()->user()->userExtra->left <= $item->kiri && auth()->user()->userExtra->right <= $item->kanan) disabled @endif">Claim {{intval($weak/$item->kiri) - cekRewardClaimed($item->id)}}
                                                                    Reward</button>
                                                            </form>
                                                        @else
                                                            <button type="submit" class="btn btn-primary btn-block"
                                                            disabled>The available BRO is not enough</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="btn btn-primary btn-block"
                                                        disabled>Reward Claimed</button>
                                                    @endif
                                                
                                            @else
                                            @endif --}}
                                                @if (intval(cekAvClaim()/$item->kiri) > 0)
                                                    @if (cekReward($item->id) == 1)
                                                    <form method="post"
                                                        action="{{ route('user.claim.rewardup', $item->id) }}"> 
                                                        @csrf
                                                    @else
                                                    <form method="post"
                                                        action="{{ route('user.claim.reward', $item->id) }}">
                                                        @csrf
                                                    @endif
                                                        
                                                        <input name="qty" type="hidden" value="{{intval(cekAvClaim()/$item->kiri)}}">
                                                        <b class="text-dark">{{intval(cekAvClaim()/$item->kiri)}} Reward Available To Claim</b>
                                                        <button type="submit"
                                                            class="btn btn-primary btn-block  @if (auth()->user()->userExtra->left <= $item->kiri && auth()->user()->userExtra->right <= $item->kanan) disabled @endif">Claim {{intval(cekAvClaim()/$item->kiri)}}
                                                            Reward</button>
                                                    </form>
                                                @else
                                                    @if (cekReward($item->id) == 1)
                                                        <button type="submit" class="btn btn-primary btn-block"
                                                        disabled>Reward Claimed, The Available Downline Not Enough To Claim Anymore.</button>
                                                    @else
                                                        <button type="submit" class="btn btn-primary btn-block"
                                                        disabled>The Available Downline Not Enough.</button>
                                                    @endif
                                                @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        {{-- @dump(auth()->user()->reward) --}}
                    </div>
                    {{-- <ul class="list-group">
                    <li class="list-group-item">150 kiri 150 kanan
                        <a href="#" class="btn btn-success btn-sm"></a>
                    </li>
                    <li class="list-group-item">999 kiri 999 kanan
                        <a href="#" class="btn btn-success btn-sm">Ambil 1 Unit Wuling Almaz</a>
                    </li>
                </ul> --}}
                </div>
            </div>
        </div>
    </div>
@endif

</div>

<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('KYC Reject Reason')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="detail_note"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
      <div class="modal-content">
        {{-- <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        </div> --}}
        <div class="modal-body px-4 py-4">
          
          
{{--             
          <p>
            <span style="font-weight: 600;font-size: 20px">
                Informasi Libur Lebaran
              </span>
<br><br>
              Selamat Hari Raya Idul Fitri bagi Sahabat Mitra Microgold yang merayakan, mohon maaf lahir dan batin 🙏 
<br>
              Selama libur Lebaran, ada beberapa informasi mengenai layanan Microgold antara lain :
              <br><br>
              - Sistem web dan perhitungan Komisi pada sistem akan berjalan seperti biasa.
              <br>
              - Team dan Layanan Microgold Libur pada tanggal 19-26 April 2023.
              <br>
              - Namun, untuk kendala dan bantuan dapat menghubungi kontak admin Microgold seperti biasa.
           
          </p> --}}
          
          {{-- <button type="button" class="btn  btn--secondary" data-dismiss="modal" style="float: right">X</button> --}}
          <button type="button" class="mb-2" data-dismiss="modal" style="float: right">x</button>
          {{-- <img src="{{asset('assets/images')}}/popup/popbanner4.png" alt="" srcset=""> --}}
          <img src="{{ getImage('assets/images/popup/popbanner04.png',  null, true)}}" class="desktop-img img-fluid">
          <img src="{{ getImage('assets/images/popup/popbanner04_2.png',  null, true)}}" class="mobile-img img-fluid">
        </div>
        
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
        'use strict';
        (function($){
            $('.detail').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('note');

                modal.find('.detail_note').html(`<p> ${feedback} </p>`);
                modal.modal('show');
            });
        })(jQuery)
    </script>
    {{-- <script type="text/javascript">
        $(window).on('load', function() {
            $('#myModal').modal('show');
        });
      </script> --}}
@endpush